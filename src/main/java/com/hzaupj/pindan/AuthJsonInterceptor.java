package com.hzaupj.pindan;

import com.hzaupj.pindan.controller.BaseJsonController;
import com.hzaupj.pindan.dao.UserDao;
import com.jfinal.aop.Interceptor;
import com.jfinal.core.ActionInvocation;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Record;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description: MX_N
 * @author: pengjian_91
 * @date: 2015/4/7
 * @version: V1.0
 */
public class AuthJsonInterceptor implements Interceptor {

    private static List<String> ignoreActionKey = new ArrayList<String>();
    static {
        ignoreActionKey.add("/user/login");
        ignoreActionKey.add("/user/register");
    }
    @Override
    public void intercept(ActionInvocation actionInvocation) {
        String actionKey = actionInvocation.getActionKey();
        Controller controller = actionInvocation.getController();
        BaseJsonController baseJsonController = null;
        if(controller instanceof BaseJsonController){
            baseJsonController = (BaseJsonController)controller;
        }
        controller.setAttr("callback", controller.getPara("callback"));
        if(ignoreActionKey.contains(actionKey)){
            actionInvocation.invoke();
            return;
        }

        String sid = controller.getCookie("sid");
        if(sid == null || sid.isEmpty()){
            baseJsonController.renderUnLoginJson();
            return;
        }

        Record user = UserDao.inquireBySid(sid);
        if(user == null){
            baseJsonController.renderUnLoginJson();
            return;
        }

        if(!actionKey.equals("/user/save") && (user.getInt("floor") == null || user.getInt("floor") <=0)){
            baseJsonController.renderErrorJson("为了方便他人取餐，楼层信息不能为空！");
            return;
        }

        MainConfig.saveLoginUser(user);
        controller.setAttr("user", user);
        controller.setAttr("accountCount", UserDao.count());
        actionInvocation.invoke();
    }
}
