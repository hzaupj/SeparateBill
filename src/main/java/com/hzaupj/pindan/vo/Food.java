package com.hzaupj.pindan.vo;

/**
 * 食物
 */
public class Food {

    private String id;
    private String name;
    private int price;
    private String picUrl;
    private int onSale = 0;
    private String hallId;
    private PlatForm platform;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    public int getOnSale() {
        return onSale;
    }

    public void setOnSale(int onSale) {
        this.onSale = onSale;
    }

    public String getHallId() {
        return hallId;
    }

    public void setHallId(String hallId) {
        this.hallId = hallId;
    }

    public PlatForm getPlatform() {
        return platform;
    }

    public void setPlatform(PlatForm platform) {
        this.platform = platform;
    }
}
