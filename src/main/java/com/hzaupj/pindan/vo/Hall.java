package com.hzaupj.pindan.vo;

import java.util.List;

/**
 * 餐厅
 *
 */
public class Hall {

    private String id;
    private String name;
    private List<String[]> disList;
    private String disInfo;
    private String bulletinInfo;
    private String otherInfo;
    private String picUrl;
    private int enable;
    private PlatForm platform;
    private int startPrice;
    private int sendPrice;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    public String getDisInfo() {
        return disInfo;
    }

    public void setDisInfo(String disInfo) {
        this.disInfo = disInfo;
    }

    public String getBulletinInfo() {
        return bulletinInfo;
    }

    public void setBulletinInfo(String bulletinInfo) {
        this.bulletinInfo = bulletinInfo;
    }

    public List<String[]> getDisList() {
        return disList;
    }

    public void setDisList(List<String[]> disList) {
        this.disList = disList;
    }

    public int getEnable() {
        return enable;
    }

    public void setEnable(int enable) {
        this.enable = enable;
    }

    public PlatForm getPlatform() {
        return platform;
    }

    public void setPlatform(PlatForm platform) {
        this.platform = platform;
    }

    @Override public String toString() {
        final StringBuilder sb = new StringBuilder("Hall{");
        sb.append("id='").append(id).append('\'');
        sb.append(", name='").append(name).append('\'');
        sb.append(", disList=").append(disList);
        sb.append(", disInfo='").append(disInfo).append('\'');
        sb.append(", bulletinInfo='").append(bulletinInfo).append('\'');
        sb.append(", otherInfo='").append(otherInfo).append('\'');
        sb.append(", picUrl='").append(picUrl).append('\'');
        sb.append(", enable=").append(enable);
        sb.append(", platform=").append(platform);
        sb.append(", startPrice=").append(startPrice);
        sb.append(", sendPrice=").append(sendPrice);
        sb.append('}');
        return sb.toString();
    }

    public String getOtherInfo() {
        return otherInfo;
    }

    public void setOtherInfo(String otherInfo) {
        this.otherInfo = otherInfo;
    }

    public int getStartPrice() {
        return startPrice;
    }

    public void setStartPrice(int startPrice) {
        this.startPrice = startPrice;
    }

    public int getSendPrice() {
        return sendPrice;
    }

    public void setSendPrice(int sendPrice) {
        this.sendPrice = sendPrice;
    }
}
