package com.hzaupj.pindan.service;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Ordering;
import com.google.common.collect.Table;
import com.hzaupj.pindan.MainConfig;
import com.hzaupj.pindan.OrderPlatform;
import com.hzaupj.pindan.algorithm.BackTracking;
import com.hzaupj.pindan.dao.UserDao;
import com.hzaupj.pindan.parse.FoodParse;
import com.hzaupj.pindan.parse.HallParse;
import com.hzaupj.pindan.vo.*;
import com.jfinal.plugin.activerecord.Record;
import jodd.bean.BeanCopy;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * MainController
 */
public class MainService {

    private static Table<Integer, String, Hall> hallTable = HashBasedTable.create(); //key = [platform, hallId]
    private static Table<String, String, Food> foodTable = HashBasedTable.create(); //key = [hallId, foodId]

    private static Map<String, PinDan> pindanMap = new HashMap<String, PinDan>(); //key = pindanId
    private static Map<String, Order> orderMap = new HashMap<String, Order>(); //key = orderId

    private static Map<String, Order> reqShareOrderMap = new HashMap<String, Order>(); //key = orderId

    private static Map<String, Map<String, SeparateBill>> reportAllMap = new HashMap<String, Map<String, SeparateBill>>();// key=pindanId

	private static Table<String, Integer, AtomicInteger> orderFoodStatisticsTable = HashBasedTable.create(); // key = [hallId, foodId]

    /**
     * 餐厅列表
     *
     * @throws Exception
     */
    public static void hallList(int opf) throws Exception {
        if (hallTable.row(opf) == null || hallTable.row(opf).isEmpty()) {
            createHallMap(opf);
        }
    }

    /**
     * 食物列表
     *
     * @throws Exception
     */
    public static void foodList(int platformId, String hallId) throws Exception {
        if (foodTable.row(hallId) == null || foodTable.row(hallId).isEmpty()) {
            // 获取foodTable
            createFoodTable(OrderPlatform.getOpf(platformId), hallId);
        }
    }

    /**
     * 获取求拼列表
     *
     * @return
     */
    public static List<Order> requestShareOrderList() {
        List<Order> resultList = new ArrayList<Order>();
        if (reqShareOrderMap == null) {
            return resultList;
        }

        Set<Map.Entry<String, Order>> orderSet = reqShareOrderMap.entrySet();
        for (Map.Entry<String, Order> orderEntry : orderSet) {
            resultList.add(orderEntry.getValue());
        }

        return resultList;
    }

    public static List<PinDan> getMyPindanList(long userId){
        List<PinDan> myPindanList = new ArrayList<PinDan>();
        if(pindanMap == null){
            return myPindanList;
        }
        for(Map.Entry<String, PinDan> map : pindanMap.entrySet()){
            PinDan pindan = map.getValue();
            if(pindan.getUserId() == userId
                    && pindan.getStatus() == 1){
                myPindanList.add(pindan);
            }
        }

        return myPindanList;
    }

    /**
     * 新增一个拼单
     *
     * @throws Exception
     */
    public static void addPindan(int platformId, String hallId, String code, boolean isTrueCode) throws Exception {
        Record user = MainConfig.getLoginUser();
        PinDan pinDan = new PinDan();
        pinDan.setId(UUID.randomUUID().toString());
        pinDan.setHall(hallTable.row(platformId).get(hallId));
        pinDan.setUserId(user.getLong("id"));
        pinDan.setStatus(1); // 开始点餐
        pinDan.setMode(1); // 宽松模式
        pinDan.setStartDate(new Date());
        if (isTrueCode) {
            pinDan.setHasPublic(false);
            pinDan.setCode(code);
        }
        pinDan.setIsAuto(1);
        pindanMap.put(pinDan.getId(), pinDan);
    }

    /**
     * 新增一个求拼订单
     *
     * @throws Exception
     */
    public static void addRequestShareOrder(String hallId, String foodId, int maxCommissionRate, String remark)
            throws Exception {
        Record user = MainConfig.getLoginUser();
        // copy food
        Food desFood = new Food();
        BeanCopy.fromBean(foodTable.get(hallId, foodId)).toBean(desFood).copy();

        Order order = new Order();
        order.setId(UUID.randomUUID().toString());
        order.setFood(desFood);
        order.setUserId(user.getLong("id"));
        order.setType(2);
        order.setMaxCommissionRate(maxCommissionRate);
        order.setStartDate(new Date());
        order.setRequestShareRemark(remark);
        reqShareOrderMap.put(order.getId(), order);
    }

    /**
     * 加入拼单（设置为同步方法，防止出现多线程同步的问题）
     * @param pinDan
     * @param order
     */
    public static synchronized void joinPindan(PinDan pinDan, Order order) {
        order.setPindanId(pinDan.getId());
        if(orderMap.get(order.getId()) == null){
            orderMap.put(order.getId(), order);
        }
        if(reqShareOrderMap.get(order.getId()) != null){
            reqShareOrderMap.remove(order.getId());
        }

        autoStopPindan(pinDan);
    }

    /**
     * 点餐
     *
     * @throws Exception
     */
    public static void order(PinDan pinDan, String hallId, String foodId) throws Exception {
        Record user = MainConfig.getLoginUser();
        // copy food
        Food desFood = new Food();
        BeanCopy.fromBean(foodTable.get(hallId, foodId)).toBean(desFood).copy();

        Order order = new Order();
        order.setId(UUID.randomUUID().toString());
        order.setFood(desFood);
        order.setPindanId(pinDan.getId());
        order.setUserId(user.getLong("id"));
        order.setType(1);
        orderMap.put(order.getId(), order);
        // 是否自动结单
        autoStopPindan(pinDan);
    }

    /**
     * 拼单详情
     *
     * @throws Exception
     */
    public static Map<String, SeparateBill> report(PinDan pindan, Hall hall, List<Order> orderList) throws Exception {
        Map<String, SeparateBill> reportMap;
        // 手动
        if(pindan.getIsAuto() == 0){
            reportMap = reportAllMap.get(pindan.getId());
        }else{
            reportMap = getReportMap(hall, orderList, pindan.getMode());
        }

        // 统计折扣
        calDis(reportMap, hall, pindan);
        // 保存拼单结果
        reportAllMap.put(pindan.getId(), reportMap);

        return reportMap;
    }

    /**
     * 删除订餐
     *
     * @throws Exception
     */
    public static void delete(Order order) throws Exception {
        orderMap.remove(order.getId());
    }

    /**
     * 删除求拼
     *
     * @throws Exception
     */
    public static void deleteReq(Order order) throws Exception {
        reqShareOrderMap.remove(order.getId());
    }

    /**
     * 删除拼单
     *
     * @param pindanId
     */
    public static void deletePindan(String pindanId) {
        // 删除订单
        List<String> orderIds = new ArrayList<String>();
        for (Order order : orderMap.values()) {
            if (pindanId.equals(order.getPindanId())) {
                orderIds.add(order.getId());
            }
        }
        for (String id : orderIds) {
            orderMap.remove(id);
        }

        // 删除报表
        reportAllMap.remove(pindanId);

        // 删除拼单列表
        pindanMap.remove(pindanId);
    }

	/**
	 * 删除拆单
	 *
	 * @param pindanId
	 */
	public static void deleteSB(String pindanId, String sbId) {
		Map<String, SeparateBill> map = reportAllMap.get(pindanId);
		if(map == null){
			return;
		}
		map.remove(sbId);
	}

	public static SeparateBill getSB(String pindanId, String sbId){
		Map<String, SeparateBill> map = reportAllMap.get(pindanId);
		if(map != null){
			return map.get(sbId);
		}
		return null;
	}

	/**
	 * 新增拆单
	 *
	 * @param pindanId
	 */
	public static String addSB(String pindanId) {
		SeparateBill sb = BackTracking.generateSB(null, new ArrayList<Order>());
		Map<String, SeparateBill> map = reportAllMap.get(pindanId);
		if(map == null){
			return null;
		}
		map.put(sb.getId(), sb);
		return sb.getId();
	}

	/**
	 * 移动到其他拆单
	 *
	 * @param order
	 * @param toSbId
	 */
	public static void moveTo(PinDan pinDan, String fromSbId,  String toSbId, Order order){
		Map<String, SeparateBill> map = MainService.getReportAllMap().get(pinDan.getId());
		if(map == null){
			return;
		}

		SeparateBill fromSb = map.get(fromSbId);
		SeparateBill toSb = map.get(toSbId);
		fromSb.getOrderList().remove(order);
		toSb.getOrderList().add(order);
	}

    /**
     * 启用拼单
     *
     * @throws Exception
     */
    public static void start(PinDan pinDan) throws Exception {
        pinDan.setStatus(1);
    }

    /**
     * 停止拼单
     *
     * @throws Exception
     */
    public static void stop(PinDan pinDan) throws Exception {
        pinDan.setStatus(0);
    }

	/**
	 * 开启自动拆单
	 *
	 * @throws Exception
	 */
	public static void openAuto(PinDan pinDan) throws Exception {
		pinDan.setIsAuto(1);
	}

	/**
	 * 关闭自动拆单
	 *
	 * @throws Exception
	 */
	public static void closeAuto(PinDan pinDan) throws Exception {
		pinDan.setIsAuto(0);
	}

    /**
     * 修改拼单模式
     *
     * @throws Exception
     */
    public static void changePindanMode(PinDan pinDan) throws Exception {
        if (pinDan.getMode() == 1) {
            pinDan.setMode(2);
        } else {
            pinDan.setMode(1);
        }
    }

    /**
     * 设置自动结单金额
     *
     * @throws Exception
     */
    public static void setAutoStopMoney(PinDan pinDan, int autoMoney) throws Exception {
        pinDan.setAutoStopMoney(autoMoney);
    }

    public static int getOrderMoney(String pindanId) {
        int num = 0;
        for (Order order : orderMap.values()) {
            if (order.getPindanId().equals(pindanId)) {
                num = num + order.getFood().getPrice();
            }
        }
        return num;
    }

    public static boolean isPartOfPindan(String pindanId, long userId) {
        for (Order order : orderMap.values()) {
            if (order.getPindanId().equals(pindanId)) {
                if(order.getUserId() == userId){
                    return true;
                }
            }
        }
        return false;
    }

    public static Map<String, PinDan> filterPindanMap(int status) {
        Map<String, PinDan> filterPindanMap = new HashMap<String, PinDan>();
        List<Map.Entry<String, PinDan>> list = new ArrayList<Map.Entry<String, PinDan>>(pindanMap.entrySet());
        for (Map.Entry<String, PinDan> mapping : list) {
            if (mapping.getValue().getStatus() == status || status == -1) {
                filterPindanMap.put(mapping.getKey(), mapping.getValue());
            }
        }
        return filterPindanMap;
    }

    public static Map<String, PinDan> orderPindanMap(Map<String, PinDan> filterMap) {
        Map<String, PinDan> orderedPindanMap = new LinkedHashMap<String, PinDan>();
        List<Map.Entry<String, PinDan>> list = new ArrayList<Map.Entry<String, PinDan>>(filterMap.entrySet());
        //然后通过比较器来实现排序
        Collections.sort(list, new Comparator<Map.Entry<String, PinDan>>() {
            //升序排序
            public int compare(Map.Entry<String, PinDan> o1,
                    Map.Entry<String, PinDan> o2) {
                return o2.getValue().getStartDate().compareTo(o1.getValue().getStartDate());
            }
        });

        for (Map.Entry<String, PinDan> mapping : list) {
            orderedPindanMap.put(mapping.getKey(), mapping.getValue());
        }

        return orderedPindanMap;
    }

    public static SeparateBill getSeparateBill(String pindanId, String disInfo) {
        Map<String, SeparateBill> map = reportAllMap.get(pindanId);
        return map.get(disInfo);
    }

    public static int getOrderNum(String pindanId) {
        int num = 0;
        for (Order order : orderMap.values()) {
            if (order.getPindanId().equals(pindanId)) {
                num++;
            }
        }
        return num;
    }

    public static Map<Long, Record> getUserMap(List<Order> orderList) {
        Map<Long, Record> userMap = new HashMap<Long, Record>();
        for (Order order : orderList) {
            Record user = UserDao.inquireById(order.getUserId());
            userMap.put(order.getUserId(), user);
        }
        return userMap;
    }

    public static boolean isNumeric(String str) {
        Pattern pattern = Pattern.compile("[0-9]*");
        Matcher isNum = pattern.matcher(str);
        if (!isNum.matches()) {
            return false;
        }
        return true;
    }

    public static List<Order> getOrderList(String pindanId, Map<String, Order> orderMap) {
        List<Order> result = new ArrayList<Order>();
        Collection<Order> values = orderMap.values();
        for (Order order : values) {
            if (pindanId.equals(order.getPindanId())) {
                result.add(order);
            }
        }
        return result;
    }

    private static void calDis(Map<String, SeparateBill> sbMap, Hall hall, PinDan pinDan) {
        Iterator<String> it = sbMap.keySet().iterator();
        while (it.hasNext()) {
            String key = it.next();
            SeparateBill sb = sbMap.get(key);
            double sumPrice = sum(sb.getOrderList());
			String[] disCounts = sb.getDiscounts();
			// 手动调整的时候
			if(pinDan.getIsAuto() == 0){
				disCounts = BackTracking.getTargetSeparateBillAmount(hall.getStartPrice(), Double.valueOf(sumPrice).intValue(), getDis(hall));
			}
            double reduceMoney = Integer.valueOf(disCounts[1]);
            reduceMoney = reduceMoney * 10;
            double disMoney = Integer.valueOf(disCounts[0]);
            disMoney = disMoney * 10;
            double dis = 1;
            if (sumPrice >= disMoney) {
                dis = (sumPrice - reduceMoney) / sumPrice;
            }
            double sumOrderPrice = sum(sb.getOrderList());
            for (Order order : sb.getOrderList()) {
                double disPrice = new BigDecimal(order.getFood().getPrice() * dis).setScale(2, BigDecimal.ROUND_CEILING)
                        .doubleValue();
                order.setDisPrice(disPrice); // 单位为分
                order.setAvgSendPrice(Double.valueOf(hall.getSendPrice()) / sumOrderPrice * order.getFood().getPrice()); // 单位为分
            }
        }
    }

	public static int sum(List<Order> list) {
		int sum = 0;
		for (Order order : list) {
			sum = sum + order.getFood().getPrice();
		}
		return sum;
	}

    private static Map<String, SeparateBill> getReportMap(Hall hall, List<Order> orderList, int mode) {
        BackTracking bt = new BackTracking();
        return bt.calc(hall, getDis(hall), orderList, mode);
    }

    private static List<String[]> getDis(Hall hall) {
        return sortList(hall.getDisList());
    }

    /**
     * 折扣力度排序要加上配送费一起计算
     * @param srcList
     * @param sendPrice
     * @return
     */
    private static List<String[]> sortList(List<String[]> srcList) {
        Ordering<String[]> disOrdering = new Ordering<String[]>() {
            @Override
            public int compare(String[] left, String[] right) {
                Double n1 = Double.parseDouble(left[0]);
                Double n2 = Double.parseDouble(right[0]);
                return n2.compareTo(n1);
            }
        };
        return disOrdering.immutableSortedCopy(srcList);
    }

    public static List<Order> sortReqShareOrderList(List<Order> orderList) {
        Ordering<Order> disOrdering = new Ordering<Order>() {
            @Override
            public int compare(Order left, Order right) {
                Integer n1 = left.getFood().getPrice() * left.getMaxCommissionRate();
                Integer n2 = right.getFood().getPrice() * right.getMaxCommissionRate();
                return n2.compareTo(n1);
            }
        };
        return disOrdering.immutableSortedCopy(orderList);
    }

    private static void createHallMap(int opf) throws IOException {
        hallTable = HashBasedTable.create();

        List<Hall> hallList = HallParse.getHallList(OrderPlatform.getOpf(opf));
        for (Hall hall : hallList) {
            hallTable.put(opf, hall.getId(), hall);
        }
    }

    private static void createFoodTable(OrderPlatform opf, String hallId) throws IOException {
        List<Food> foodList = FoodParse.getFoodList(opf, hallId);
        for (Food food : foodList) {
            foodTable.put(hallId, food.getId(), food);
        }
    }

    private static void autoStopPindan(PinDan pindan) {
        if (pindan.getAutoStopMoney() <= 0) {
            return;
        }
        List<Order> orderList = getOrderList(pindan.getId(), orderMap);
        double sumMoney = sum(orderList);
        // 拼单总额超出设定金额，自动停单
        if (sumMoney >= pindan.getAutoStopMoney() * 10) {
            pindan.setStatus(0);
        }
    }

    public static void setPindanMap(Map<String, PinDan> temp) {
        pindanMap = temp;
    }

    public static Map<String, PinDan> getPindanMap() {
        return pindanMap;
    }

    public static void setHallTable(Table<Integer, String, Hall> temp) {
        hallTable = temp;
    }

    public static Table<Integer, String, Hall> getHallTable() {
        return hallTable;
    }

    public static void setFoodTable(Table<String, String, Food> temp) {
        foodTable = temp;
    }

    public static Table<String, String, Food> getFoodTable() {
        return foodTable;
    }

    public static void setOrderMap(Map<String, Order> temp) {
        orderMap = temp;
    }

    public static Map<String, Order> getOrderMap() {
        return orderMap;
    }

    public static Map<String, Order> getReqShareOrderMap() {
        return reqShareOrderMap;
    }

    public static void setReqShareOrderMap(Map<String, Order> reqShareOrderMap) {
        MainService.reqShareOrderMap = reqShareOrderMap;
    }
    public static Map<String, Map<String, SeparateBill>> getReportAllMap() {
        return reportAllMap;
    }

    public static void setReportAllMap(
			Map<String, Map<String, SeparateBill>> reportAllMap) {
        MainService.reportAllMap = reportAllMap;
    }

	public static Table<String, Integer, AtomicInteger> getOrderFoodStatisticsTable() {
		return orderFoodStatisticsTable;
	}

	public static void setOrderFoodStatisticsTable(
			Table<String, Integer, AtomicInteger> orderFoodStatisticsTable) {
		MainService.orderFoodStatisticsTable = orderFoodStatisticsTable;
	}
}