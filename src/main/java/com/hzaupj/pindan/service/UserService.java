package com.hzaupj.pindan.service;

import com.hzaupj.pindan.MainConfig;
import com.hzaupj.pindan.dao.UserDao;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;

import java.util.UUID;

/**
 * MainController
 */
public class UserService {

	/**
	 * 登录
	 * @throws Exception
	 */
	public static String login(Record user) throws Exception {
		String sid = UUID.randomUUID().toString();
		UserDao.saveSid(user.getLong("id"), sid);
		return sid;
	}

	/**
	 * 注册
	 * @throws Exception
	 */
	public static String register(String account, String name, String password, Integer floor) throws Exception {
		return UserDao.addUser(account, name, password, floor);
	}

	/**
	 * 用户信息
	 * @throws Exception
	 */
	public static Record getUserInfo() throws Exception {
		return MainConfig.getLoginUser();
	}

	/**
	 * 保存用户信息
	 * @throws Exception
	 */
	public static void save(Record user, String name, String alipayNo, String weixinNo,
            String alipayCodeUrl, Integer floor) throws Exception {
		user.set("name", name);
		user.set("floor", floor);
		user.set("alipay_no", alipayNo);
		user.set("weixin_no", weixinNo);
		user.set("alipay_code_url", alipayCodeUrl);
		Db.update("user", user);
	}

	/**
	 * 修改密码
	 * @throws Exception
	 */
	public static void updatePassword(Record user, String new_password) throws Exception {
		user.set("password", new_password);
		Db.update("user", user);
	}
}





