package com.hzaupj.pindan.json;

import java.util.List;

/**
 * @Description: MX_N
 * @author: pengjian_91
 * @date: 2015/4/3
 * @version: V1.0
 */
public class JShopCart {
    private List<JFood> foods;
    private int poi; // hallId

    public int getPoi() {
        return poi;
    }

    public void setPoi(int poi) {
        this.poi = poi;
    }

    public List<JFood> getFoods() {
        return foods;
    }

    public void setFoods(List<JFood> foods) {
        this.foods = foods;
    }
}
