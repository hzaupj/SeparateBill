/*global ngNice,moment*/
(function () {
    "use strict";

    // var spbFilters = angular.module('spbServices', ['ngResource']);

    spbApp.filter("fomartPrice", [function () {
        return function (input) {
            var num = Number(input)/10;
            return '￥'+ num.toFixed(2) ;
        }
    }])
    .filter("fomartNumber", [function () {
        return function (input) {
            var num = Number(input);
            return num.toFixed(2) ;
        }
    }])
    .filter('cut', function () {
        return function(value, wordwise, max, tail) {
            if (!value) return '';

            max = parseInt(max, 10);
            if (!max) {
                console.log(max);
                console.log(value);
                return value;
            }
            if (value.length <= max) return value;

            value = value.substr(0, max);
            if (wordwise) {
            var lastspace = value.lastIndexOf(' ');
                if (lastspace != -1) {
                value = value.substr(0, lastspace);
            }
        }
        return value + (tail || ' …');
        };
    });
})();
