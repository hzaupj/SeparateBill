package com.hzaupj.pindan.task;

import com.google.common.collect.Table;
import com.hzaupj.pindan.OrderPlatform;
import com.hzaupj.pindan.parse.HallParse;
import com.hzaupj.pindan.service.MainService;
import com.hzaupj.pindan.vo.Hall;
import org.joda.time.DateTime;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;

/**
 * @Description: MX_N
 * @author: pengjian_91
 * @date: 2015/4/23
 * @version: V1.0
 */
public class HallListTask implements Job {
	private static Logger logger = LoggerFactory.getLogger(HallListTask.class);
    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        for(OrderPlatform opf : OrderPlatform.values()){
			logger.info("获取[" + opf.getName() + "] 餐厅列表 start " + DateTime.now().toString("yyyy-MM-dd HH:mm:ss"));
            updateHallTable(opf);
			logger.info("获取[" + opf.getName() +"] 餐厅列表 end " + DateTime.now().toString("yyyy-MM-dd HH:mm:ss"));
        }
    }

    private void updateHallTable(OrderPlatform opf){
        Table<Integer, String, Hall> hallTable = MainService.getHallTable();
        List<Hall> hallList = null;
        try {
            hallList = HallParse.getHallList(opf);
        } catch (IOException e) {
            e.printStackTrace();
        }

		if(hallList == null){
			return;
		}

        for (Hall hall : hallList) {
            hallTable.put(opf.getCode(), hall.getId(), hall);
        }
        MainService.setHallTable(hallTable);
    }
}
