package com.hzaupj.pindan.algorithm;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import com.hzaupj.pindan.service.MainService;
import com.hzaupj.pindan.vo.Hall;
import com.hzaupj.pindan.vo.Order;
import com.hzaupj.pindan.vo.SeparateBill;

import java.util.*;

/**
 * Created by pengjian_91 on 2015/3/18.
 */
public class BackTracking {

    public Map<String, SeparateBill> calc(Hall hall, List<String[]> discountInfoList, List<Order> srcOrder, int mode){
        Map<String, SeparateBill> resultMap = new HashMap<String, SeparateBill>();

        int num = srcOrder.size();
		int totalSum = MainService.sum(srcOrder);
		System.out.println("totalSum = " + totalSum);

		Order[] srcOrderItems = srcOrder.toArray(new Order[num]);

        String[] targetSumInfo = getTargetSeparateBillAmount(hall.getStartPrice(), totalSum, discountInfoList);
        int targetSum = Integer.valueOf(targetSumInfo[0])*10;
        while (totalSum - targetSum >= 0){
            Order[] answers = getRealAnswer(srcOrderItems, targetSum);
			SeparateBill sb = generateSB(targetSumInfo, coverToList(answers));
            resultMap.put(sb.getId(), sb);

            srcOrderItems = delOrderItems(srcOrderItems, answers);
            totalSum = totalSum - MainService.sum(Arrays.asList(answers));

            targetSumInfo = getTargetSeparateBillAmount(hall.getStartPrice(), totalSum, discountInfoList);
            targetSum = Integer.valueOf(targetSumInfo[0])*10;
        }

        if(srcOrderItems.length > 0){
			SeparateBill sb = generateSB(targetSumInfo, coverToList(srcOrderItems));
            if(mode == 2){// 严格模式
                resultMap.put(sb.getId(), sb);
            }else{// 宽松模式
                if(resultMap.isEmpty()){
                    resultMap.put(sb.getId(), sb);
                }else{
                    aaResultMap(resultMap, srcOrderItems);
                }
            }
        }
        return resultMap;
    }

	public static SeparateBill generateSB(String[] disInfos, List<Order> orderList){
		SeparateBill sb = new SeparateBill();
		String sbId = UUID.randomUUID().toString();
		sb.setId(sbId);
		sb.setDiscounts(disInfos);
		sb.setOrderList(orderList);
		return sb;
	}

    private void aaResultMap(Map<String, SeparateBill> resultMap, Order[] srcOrderItems) {
        int length = srcOrderItems.length;
        int i=0;
        while(i<length){
            Iterator<String> it = resultMap.keySet().iterator();
            while(it.hasNext()){
                if(i>=length){
                    break;
                }
                String key = it.next();
                resultMap.get(key).getOrderList().add(srcOrderItems[i]);
                i++;
            }
        }
    }

    private List<Order> coverToList(Order[] orders){
        List<Order> list = new ArrayList<Order>();
        for(Order order : orders){
            list.add(order);
        }
        return list;
    }

    /**
     * 获取真正的结果
     *
     * 因为有可能返回结果小于targetSum，所以要不断尝试放大targetSum直到大于等于targetSum
     *
     * @param srcOrderItems
     * @param targetSum
     * @return
     */
    private Order[] getRealAnswer(Order[] srcOrderItems, int targetSum) {
        int tempSum = targetSum;
        int srcSum = 0;
        Order[] resultOrderItems = null;
        while (srcSum < targetSum){
            resultOrderItems = getAnswer(srcOrderItems, tempSum);
            srcSum = MainService.sum(Arrays.asList(resultOrderItems));
			tempSum++;
        }

        return resultOrderItems;
    }

    private Order[] delOrderItems(Order[] srcOrderItems, Order[] answers) {
        for (int i=0; i<answers.length; i++){
            for (int j=0; j<srcOrderItems.length; j++){
                if(srcOrderItems[j] == null){
                    continue;
                }
                if(answers[i].getId() == srcOrderItems[j].getId()){
                    srcOrderItems[j] = null;
                    break;
                }
            }
        }
        List<Order> result = new ArrayList<Order>();
        for (Order srcItem : srcOrderItems){
            if(srcItem == null){
                continue;
            }
            result.add(srcItem);
        }

        return result.toArray(new Order[result.size()]);
    }

    private Order[] getAnswer(Order[] orderItems, int targetSum) {
        Table<Integer,Integer,Integer> foodTable= HashBasedTable.create();
        int i;
        Order order;
        for(i=0; i<=targetSum; i++){
            for(int j=0; j<orderItems.length; j++){
                order = orderItems[j];
                if(order.getFood().getPrice() > i){
                    if(j == 0){
                        foodTable.put(j,i,0);
                    }else{
                        foodTable.put(j,i,foodTable.get(j-1,i));
                    }
                }else{
                    int sum =0;
                    if(j == 0){
                        foodTable.put(j, i, order.getFood().getPrice());
                        continue;
                    }else{
                        sum = foodTable.get(j-1, i-order.getFood().getPrice()) + order.getFood().getPrice();
                    }
                    int max = foodTable.get(j-1, i) > sum ? foodTable.get(j-1,i) : sum;
                    foodTable.put(j,i,max);
                }
            }
        }

        List<Order> answers = new ArrayList<Order>();
        int curSum = targetSum;
        for(i=orderItems.length-1;i>=0;i--){
            order = orderItems[i];
            if(curSum == 0){
                break;
            }
            if(i==0 && curSum>0){
                answers.add(order);
                break;
            }
            if(curSum-order.getFood().getPrice() < 0){
                continue;
            }
            if(foodTable.get(i,curSum) - foodTable.get(i-1, curSum-order.getFood().getPrice()) == order.getFood().getPrice()){
                answers.add(order);
                curSum -= order.getFood().getPrice();
            }
        }
        return answers.toArray(new Order[answers.size()]);
    }

    /**
     * 计算最优拆单金额
     * @param startPrice 起送价
     * @param totalSum 当前拼单总金额
     * @param discountInfoList 满减折扣列表
     * @return
     */
    public static String[] getTargetSeparateBillAmount(int startPrice, int totalSum, List<String[]> discountInfoList) {
        // 如果没有满减折扣，则没有拆单金额
        if(discountInfoList == null || discountInfoList.isEmpty()){
            return new String[]{String.valueOf(totalSum) ,"0"};
        }

        String[] results = new String[2];

        // discountInfoList是按照折扣力度从大到小排列
        for(String[] discounts : discountInfoList){
            results[0] = discounts[0];
            results[1] = discounts[1];
            int temp = Integer.valueOf(results[0])*10;
            if(totalSum >= temp){
                // 拼单金额大于当前折扣金额后，还需要比较起送价格是否大于折扣金额，是的话需要按照起送价格拆单
                if(startPrice > temp){
                    results[0] = String.valueOf(startPrice/10);
                }
                return results;
            }
        }
        return results;
    }
}
