'user strict';
var orderModule = angular.module('orderModule', ['orderControllers', 'spbServices', 'ui.bootstrap','angular-sortable-view']);
var orderControllers = angular.module('orderControllers', ['ngResource']);

//拼单列表
orderControllers.controller('orderListCtrl', ['$scope', '$state', '$stateParams', 'Api', 'SweetAlert', 
    function($scope, $state, $stateParams, Api, SweetAlert) {
        console.log("注入拼单列表模块");
        $scope.orders = [];
        $scope.hasCurUser = false;
        $scope.hasOwner = false;
        $scope.curUserInfo = {};
        Api.order.list.query({stateId: '-1'})
        .$promise.then(
            function(data){
                if(data.flag === "SUCCESS"){
                    $scope.orders = data.result.pindanMap;
                    $scope.partPD = data.result.myPartPindanMap;
                    $scope.startPD = data.result.myStartPindanMap;
                    $scope.user = data.result.userMap;
                    $scope.orderInfo = data.result.orderInfoMap;

                    //获取当前登录用户
                    var curUser = Api.getAccount();
                    //判断当前用户是否在订单用户中
                    angular.forEach($scope.user, function(item,index){
                        if(item.columns.account == curUser){
                            $scope.hasCurUser = true;
                            $scope.curUserInfo.account = item.columns.account;
                            $scope.curUserInfo.id = item.columns.id;
                        }
                    });
                }else if(data.flag === "UNLOGIN"){
                    $state.go("login");
                }
             },
            function(data) {
                console.log(data.status,"请求出错");
            }
        );
        //根据平台显示
        $scope.byPlatform = function(id) {
            return 1;
        };
        //验证拼单口令
        $scope.verifyPd = function(platformId,hallId,pindanId,hasPublic,code){
            swal({
                title: "请输入拼单口令",   
                text: "",   
                type: "input",   
                showCancelButton: true,
                closeOnCancel: true,
                closeOnConfirm: true,
                allowOutsideClick: true,
                confirmButtonText: '确定',
                cancelButtonText: '取消',   
                animation: "slide-from-top",   
                inputPlaceholder: "拼单口令" 
            }, function(inputValue){
               if (inputValue === false) return false;      
               if (inputValue === "" || inputValue.length !== 4 || inputValue !== code) {
                    swal.showInputError("口令错误，请输入4位数字的口令");     
                    return false   
                }
                $state.go('book',{platformId: platformId,hallId: hallId, pindanId: pindanId, hasPublic: hasPublic});
            })
        };
        // 删除拼单
        $scope.deleteOrder = function(pindanId){
            SweetAlert.swal({   
                title: "删除拼单",   
                text: "你确定要删除此拼单吗?",   
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#DD6B55",   
                confirmButtonText: "删除",  
                cancelButtonText: '取消',  
                closeOnConfirm: false   
            }, function(){  
                Api.order.remove.query({
                    pindanId:pindanId,
                })
                .$promise.then(
                    function(data){
                        var flag = data.flag;
                        if(flag === "SUCCESS"){
                            SweetAlert.swal({title:"删除成功!", text:"", type:"success"},function(){
                                // delete $scope.orders[pindanId]; 
                                $state.go($state.current,{},{reload: true});
                            });
                        }else if(flag === "FAIL"){
                            SweetAlert.swal(data.msg, "", "error");   
                        }
                    },
                    function(data) {
                        SweetAlert.swal("请求出错!", "", "error");   
                    }
                );
            });
        };
        // 继续点餐
        $scope.start = function(pindanId){
            Api.order.start.query({pindanId: pindanId})
            .$promise.then(
                function(data) {
                    if(data.flag === "SUCCESS"){
                        $state.go($state.current,{},{reload: true});
                    }
                },
                function(data) {
                    console.log(data,"请求出错");

                }
            );
        };

        // 停止点餐
        $scope.stop = function(pindanId){
            Api.order.stop.query({pindanId: pindanId})
            .$promise.then(
                function(data) {
                    if(data.flag === "SUCCESS"){
                        $state.go($state.current,{},{reload: true});
                    }
                },
                function(data) {
                    console.log(data,"请求出错");
                }
            );
        };

    }
]);


//拼单详情
orderControllers.controller('orderDetailCtrl', ['$scope', '$state', '$stateParams', 'SweetAlert', 'Api', '$filter',
    function($scope, $state, $stateParams, SweetAlert, Api, $filter) {
        console.log("注入拼单详情模块");
        var cartLen = 0;
        $scope.hasCurUser = false;
        $scope.hasOwner = false;
        $scope.curUserInfo = {};
        $scope.order = {};
        $scope.order.cart = [];
        $scope.order.mode;
        $scope.order.isAuto = 1;
        $scope.order.state = true;

        Api.order.detail.query({
            platformId:$stateParams.platformId,
            hallId:$stateParams.hallId,
            pindanId:$stateParams.pindanId,
            foodId:$stateParams.foodId,
        })
        .$promise.then(
            function(data){
                if(data.flag === "SUCCESS"){
                    $scope.hall = data.result.hall
                    $scope.pindan = data.result.pindan;
                    $scope.owner = data.result.owner;
                    $scope.user = data.result.userMap;
                    $scope.orders = data.result.map;
                    //重组拼单详情列表数据
                    angular.forEach($scope.orders, function(item,index){
                        var username, orderList;
                        for(var k in item.orderList){
                            item.orderList[k].SbId = index;
                        }
                        $scope.order.cart.push({
                            SbId : index,
                            foodList : item.orderList,
                            sendPrice : $scope.hall.sendPrice,
                        });
                    });
                    $scope.order["sendPrice"] = $scope.pindan.hall.sendPrice;
                    // 每项订餐的配送费
                    $scope.itemSendPrice = function(item,list){
                        var price = 0;
                        price = $scope.hall.sendPrice / $scope.foodPrice(list) * item.disPrice;
                        return price;
                    };

                    // 每项订餐的佣金
                    $scope.itemCommPrice = function(item){
                        var price = 0;
                        price = item.food.price * (item.maxCommissionRate / 100);
                        return price;
                    };

                    // 订单所有食物打折后总价      
                    $scope.foodPrice = function(list){
                        var price = 0;
                        angular.forEach(list.foodList, function(item,index){
                            price += item.disPrice;
                        })
                        return price;
                        
                    }; 

                    // 订单内单项食物打折后+配送费+佣金总价       
                    $scope.itemTotalPrice = function(item,list){
                        var price = 0;
                        price = item.disPrice + $scope.itemCommPrice(item);
                        return price;
                    }; 

                    // 订单总佣金
                    $scope.cartCommPrice = function(list){
                        var price = 0;
                        angular.forEach(list.foodList, function(item,index){
                            price += $scope.itemCommPrice(item);
                        })
                        return price;
                    }

                    // 订单总价包含配送与佣金
                    $scope.cartTotal = function(list){
                        var price = 0, totalPrice = 0;
                        angular.forEach(list.foodList, function(item,index){
                            price = $scope.foodPrice(list) + $scope.hall.sendPrice + $scope.cartCommPrice(list);
                        });
                        return price;
                    }

                    // 所有费用
                    $scope.allTotalPrice = function(){
                        var price = 0, totalPrice = 0;
                        angular.forEach($scope.order.cart, function(item,index){
                            price += $scope.foodPrice(item) + $scope.hall.sendPrice  + $scope.cartCommPrice(item);
                        });
                        return price;
                    }


                    var curUser = Api.getAccount();
                    // 判断当前用户是否在订单用户中
                    angular.forEach($scope.user, function(item,index){
                        if(item.columns.account == curUser){
                            $scope.hasCurUser = true;
                            $scope.curUserInfo.account = item.columns.account;
                            $scope.curUserInfo.id = item.columns.id;
                        }
                    });

                    //判断当前用户是否是拼单发起人
                    if($scope.pindan.userId == $scope.curUserInfo.id){
                        $scope.hasOwner = true;
                    }
                    
                    if(data.result.pindan.mode === 1){
                        $scope.order.mode = true;
                    }
                    if(data.result.pindan.status === 0){
                        $scope.order.state = false;
                    }
                    if(data.result.pindan.isAuto === 0){
                        $scope.order.isAuto = false;
                    }
              
                }
             },
            function(data) {
                console.log(data.status,"请求出错");
            }
        );
        
        // console.log($scope.order)
        // 验证拼单口令
        $scope.verifyPd = function(platformId,hallId,pindanId,hasPublic,code){
            swal({
                title: "请输入拼单口令",   
                text: "",   
                type: "input",   
                showCancelButton: true,
                closeOnCancel: true,
                closeOnConfirm: true,
                allowOutsideClick: true,
                confirmButtonText: '确定',
                cancelButtonText: '取消',   
                animation: "slide-from-top",   
                inputPlaceholder: "拼单口令" 
            }, function(inputValue){
               if (inputValue === false) return false;      
               if (inputValue === "" || inputValue.length !== 4 || inputValue !== code) {
                    swal.showInputError("口令错误，请输入4位数字的口令");     
                    return false;
                }
                $state.go('book',{platformId: platformId,hallId: hallId, pindanId: pindanId, hasPublic: hasPublic});
            })
        }
        // 设置结算金额
        $scope.setQuota = function(){
            swal({
                title: "设置结算金额",   
                text: "达到结算金额，系统将停止点餐，为0则取消自动结算。",   
                type: "input",   
                showCancelButton: true,
                closeOnCancel: true,
                closeOnConfirm: false,
                allowOutsideClick: true,
                confirmButtonText: '确定',
                cancelButtonText: '取消',   
                animation: "slide-from-top",   
                inputPlaceholder: "请输入自动结算金额" 
            }, function(inputValue){
               if (inputValue === false) return false;      
               if (inputValue === "" || isNaN(inputValue)) {
                    swal.showInputError("请输入数字");     
                    return false   
                }
                Api.order.quota.query({ pindanId:$stateParams.pindanId,autoMoney:inputValue})
                .$promise.then(
                    function(data){
                        var flag = data.flag;
                        if(flag === "SUCCESS"){
                            SweetAlert.swal({title:data.msg, text:"", type:"success"},function(){
                                $state.go($state.current,{},{reload: true});
                            });
                        }else if(flag === "FAIL"){
                            SweetAlert.swal(data.msg, "", "error");   
                        }
                    },
                    function(data) {
                        SweetAlert.swal("请求出错!", "", "error");   
                    }
                );
            })

        }
        // 删除拼单
        $scope.deleteOrder = function(){
            SweetAlert.swal({   
                title: "删除拼单",   
                text: "你确定要删除此拼单吗?",   
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#DD6B55",   
                confirmButtonText: "删除",  
                cancelButtonText: '取消',  
                closeOnConfirm: false   
            }, function(){  
                Api.order.remove.query({
                    pindanId:$stateParams.pindanId,
                })
                .$promise.then(
                    function(data){
                        var flag = data.flag;
                        if(flag === "SUCCESS"){
                            SweetAlert.swal({title:"删除成功!", text:"", type:"success"},function(){
                                $state.go('order');
                            });
                        }else if(flag === "FAIL"){
                            SweetAlert.swal(data.msg, "", "error");   
                        }
                    },
                    function(data) {
                        SweetAlert.swal("请求出错!", "", "error");   
                    }
                );
            });
        };

        // 删除订餐
        $scope.removeFood = function(food,orderId,listId){
            angular.forEach($scope.order.cart, function(item,index){
                if(item.id === listId){
                    Api.order.removeFood.query({
                        orderId:orderId,
                    })
                    .$promise.then(
                        function(data){
                            if(data.flag === "SUCCESS"){
                                item.foodList.splice(item.foodList.indexOf(food),1)
                            }
                        },
                        function(data) {
                            SweetAlert.swal("请求出错!", "", "error");   
                        }
                    )
                }
            });
        };

        // 修改模式
        $scope.changeMode = function(){
            Api.order.mode.query({pindanId: $stateParams.pindanId})
            .$promise.then(
                function(data) {
                    if(data.flag === "SUCCESS"){
                        $scope.order.mode = !$scope.order.mode;
                        $state.go($state.current,{},{reload: true});
                    }
                },
                function(data) {
                    console.log(data,"请求出错");
                }
            );
        };

        // 手动调整
        $scope.closeAuto = function(){
            Api.order.closeAuto.query({pindanId: $stateParams.pindanId})
            .$promise.then(
                function(data) {
                    console.log(data)
                    if(data.flag === "SUCCESS"){
                        $scope.order.isAuto = 0;
                        $state.go($state.current,{},{reload: true});
                    }
                },
                function(data) {
                    console.log(data,"请求出错");
                }
            );
        }

        // 自动拆单
        $scope.openAuto = function(){
            Api.order.openAuto.query({pindanId: $stateParams.pindanId})
            .$promise.then(
                function(data) {
                    console.log(data)
                    if(data.flag === "SUCCESS"){
                        $scope.order.isAuto = 1;
                        $state.go($state.current,{},{reload: true});
                    }
                },
                function(data) {
                    console.log(data,"请求出错");
                }
            );
        }

        //新增拆单
        $scope.addCart = function(){
             Api.order.addSB.query({pindanId: $stateParams.pindanId})
             .$promise.then(
                function(data) {
                    if(data.flag === "SUCCESS"){
                        $scope.newCart = [{
                            disPrice: 0,
                            maxCommissionRate: 3,
                            food:{
                                price: 0
                            }
                        }];
                        $scope.order.cart.push({
                            SbId : data.result.sbId,
                            foodList : $scope.newCart,
                            sendPrice : $scope.hall.sendPrice
                        });

                    }
                },
                function(data) {
                    console.log(data,"请求出错");
                }
            );
        }
        //删除拆当
        $scope.removeCart = function(list){
            console.log(list);
            if(!list.foodList || !list.foodList[0] || list.foodList[0].disPrice === 0){
                Api.order.deleteSB.query({pindanId: $stateParams.pindanId, SbId:list.SbId})
                 .$promise.then(
                    function(data) {
                        if(data.flag === "SUCCESS"){
                            $scope.order.cart.splice($scope.order.cart.indexOf(list), 1);
                        }else{
                            SweetAlert.swal("删除失败", "", "error");   
                            $state.go($state.current,{},{reload: true});
                        }
                    },
                    function(data) {
                        console.log(data,"请求出错");
                    }
                );
            }else{
                 SweetAlert.swal("删除失败", "如要删除此拆单，请将拆单内的订单移至其他拆单。", "error");   
            }
        }

        // 拖拽拼单
        $scope.moveSB = function(item,partFrom,partTo,indexFrom,indexTo,partToParent, partFromParent){
            // 查找拖动项的拆单id

            var toSbId = partToParent.element.attr("data-id"), 
                fromSbId = partFromParent.element.attr("data-id"),
                orderId = item.id,
                pindanId = item.pindanId;

            if (toSbId == fromSbId) {
                return false;
            };
            Api.order.moveSB.query({
                pindanId: pindanId,
                orderId: orderId,
                toSbId: toSbId,
                fromSbId: fromSbId,
            })
            .$promise.then(
                function(data) {
                    console.log(data);
                    if(data.flag === "SUCCESS"){
                        $state.go($state.current,{},{reload: true});
                    }
                },
                function(data) {
                    console.log(data,"请求出错");
                }
            );
        }

        


        // 继续点餐
        $scope.start = function(){
            Api.order.start.query({pindanId: $stateParams.pindanId})
            .$promise.then(
                function(data) {
                    if(data.flag === "SUCCESS"){
                        $scope.order.state = !$scope.order.state;
                        $state.go($state.current,{},{reload: true});
                    }
                },
                function(data) {
                    console.log(data,"请求出错");

                }
            );
        };

        // 停止点餐
        $scope.stop = function(){
            Api.order.stop.query({pindanId: $stateParams.pindanId})
            .$promise.then(
                function(data) {
                    if(data.flag === "SUCCESS"){
                        $state.go($state.current,{},{reload: true});
                        $scope.order.state = !$scope.order.state;
                    }
                },
                function(data) {
                    console.log(data,"请求出错");
                }
            );
        };

        $scope.remarked = {};
        // 设置备注
        $scope.toggle = function(idx){
            $scope.remarked[idx] = !$scope.remarked[idx];
        }
        $scope.updateRemark = function(orderId,remark,idx){
            $scope.remarked[idx] = !$scope.remarked[idx];
            Api.order.updateRemark.query({
                orderId: orderId, 
                remark: remark
            })
            .$promise.then(
                function(data) {
                    if(data.flag !== "SUCCESS"){
                        SweetAlert.swal({title:"提交时出错!", text:"", type:"error"},function(){
                            $state.go($state.current,{},{reload: true});
                        });
                    }
                },
                function(data) {
                    console.log(data,"请求出错");
                }
            );
        };
    }
]);


//餐厅页面-点餐
orderControllers.controller('bookCtrl', ['$scope', '$state', '$stateParams', 'Api',
    function($scope, $state, $stateParams, Api) {
        console.log("注入点餐餐厅模块");
        Api.order.book.query({
            platformId:$stateParams.platformId,
            hallId:$stateParams.hallId,
            pindanId:$stateParams.pindanId,
            hasPublic:$stateParams.hasPublic
        })
        .$promise.then(
            function(data){
                if(data.flag === "SUCCESS"){
                    $scope.shop = data.result;

                }
             },
            function(data) {
                console.log(data.status,"请求出错");
            }
        );
        $scope.addFood = function(foodId){
            Api.order.addFood.query({ 
                platformId:$stateParams.platformId,
                hallId:$stateParams.hallId,
                pindanId:$stateParams.pindanId,
                foodId: foodId, 
            })
            .$promise.then(
                function(data) {
                    if(data.flag === "SUCCESS"){
                        $state.go('orderDetail',{platformId: $stateParams.platformId,hallId:$stateParams.hallId, pindanId:$stateParams.pindanId,},{reload: true});
                    }
                },
                function(data) {
                    console.log(data,"请求出错");
                }
            );
        }
    }
]);
