package com.hzaupj.pindan.vo;

import java.util.Date;

/**
 * @Description: MX_N
 * @author: pengjian_91
 * @date: 2015/4/7
 * @version: V1.0
 */
public class PinDan {
    private String id;
    private Hall hall;
    private long userId;
    private int status; // 0-停止点餐 1-开始点餐
    private int mode; // 1-宽松模式 2-严格模式
    private Date startDate; // 发起时间
    private String code; // 如果是私密，需要验证码
    private boolean hasPublic = true; // 是否公开
    private int autoStopMoney; // 自动停止点餐的金额
    private int isAuto; // 是否自动拆单 0-否 1-是

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
		// 开启点餐
		if(status == 1){
			this.isAuto = 1;
		}
    }

    public Hall getHall() {
        return hall;
    }

    public void setHall(Hall hall) {
        this.hall = hall;
    }

    public int getMode() {
        return mode;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public boolean isHasPublic() {
        return hasPublic;
    }

    public void setHasPublic(boolean hasPublic) {
        this.hasPublic = hasPublic;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getAutoStopMoney() {
        return autoStopMoney;
    }

    public void setAutoStopMoney(int autoStopMoney) {
        this.autoStopMoney = autoStopMoney;
    }

    @Override
    public String toString() {
        return "PinDan{" +
                "id='" + id + '\'' +
                ", hall=" + hall +
                ", userId=" + userId +
                ", status=" + status +
                ", mode=" + mode +
                ", startDate=" + startDate +
                ", code='" + code + '\'' +
                ", hasPublic=" + hasPublic +
                ", autoStopMoney=" + autoStopMoney +
                '}';
    }

    public int getIsAuto() {
        return isAuto;
    }

    public void setIsAuto(int isAuto) {
        this.isAuto = isAuto;
		// 切换为手工调整的时候，自动停止点餐
		if(isAuto == 0){
			this.status = 0;
		}
    }
}
