package com.hzaupj.pindan.task;

import com.google.common.collect.Table;
import com.hzaupj.pindan.OrderPlatform;
import com.hzaupj.pindan.parse.FoodParse;
import com.hzaupj.pindan.service.MainService;
import com.hzaupj.pindan.vo.Food;
import com.hzaupj.pindan.vo.Hall;
import org.joda.time.DateTime;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;

/**
 * @Description: MX_N
 * @author: pengjian_91
 * @date: 2015/4/23
 * @version: V1.0
 */
public class FoodListTask implements Job {
	private static Logger logger = LoggerFactory.getLogger(FoodListTask.class);

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        Table<String, String, Food> foodTable = MainService.getFoodTable();
        if(foodTable == null || foodTable.isEmpty()){
            return;
        }

        for(String hallId: foodTable.rowKeySet()){
            Table<Integer, String, Hall> hallTable = MainService.getHallTable();
            for(OrderPlatform opf : OrderPlatform.values()){
                Hall hall = hallTable.get(opf.getCode(), hallId);
                if(hall != null) {
                    logger.info("更新[" + opf.getName() +"] 的 ["+hall.getName()+"] 餐厅的foodList！ start " + DateTime.now().toString("yyyy-MM-dd HH:mm:ss"));
                    updateFoodTable(opf, hallId);
                    logger.info("更新[" + opf.getName() +"] 的 ["+hall.getName()+"] 餐厅的foodList！ end " + DateTime.now().toString("yyyy-MM-dd HH:mm:ss"));
                }
            }
        }
    }

    private void updateFoodTable(OrderPlatform opf, String hallId) {
        Table<String, String, Food> foodTable = MainService .getFoodTable();
        List<Food> foodList = null;
        try {
            foodList = FoodParse.getFoodList(opf, hallId);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if(foodList == null){
            return;
        }
        for (Food food : foodList) {
            foodTable.put(hallId, food.getId(), food);
        }
    }
}
