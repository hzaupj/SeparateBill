package com.hzaupj.pindan;

import com.hzaupj.pindan.controller.MainTemplateController;
import com.hzaupj.pindan.controller.UserTemplateController;
import com.jfinal.config.*;
import com.jfinal.core.JFinal;
import com.jfinal.ext.plugin.quartz.QuartzPlugin;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.c3p0.C3p0Plugin;
import org.beetl.ext.jfinal.BeetlRenderFactory;

/**
 * API引导式配置
 */
public class MainConfig extends JFinalConfig {

	private static ThreadLocal<Record> loginUser = new ThreadLocal<Record>();
	public static int MAX_COOKIE_TIME = 365*24*60*60;
	public static void saveLoginUser(Record user){
		loginUser.set(user);
	}

	public static Record getLoginUser(){
		return loginUser.get();
	}

	/**
	 * 配置常量
	 */
	public void configConstant(Constants me) {
		// 集成beetl模板引擎
		loadPropertyFile("db_config.properties");
		me.setMainRenderFactory(new BeetlRenderFactory());
		me.setDevMode(true);
	}
	
	/**
	 * 配置路由
	 */
	public void configRoute(Routes me) {
		me.add("/main", MainTemplateController.class);
//		me.add("/main", MainJsonController.class);
		me.add("/user", UserTemplateController.class);
//		me.add("/user", UserJsonController.class);
	}
	
	/**
	 * 配置插件
	 */
	public void configPlugin(Plugins me) {
		// 数据库插件
		C3p0Plugin c3p0Plugin = new C3p0Plugin(getProperty("jdbcUrl"), getProperty("user"), getProperty("password").trim());
		me.add(c3p0Plugin);

		// dao插件
		ActiveRecordPlugin arp = new ActiveRecordPlugin(c3p0Plugin);
		me.add(arp);

		// 定时器插件
		QuartzPlugin configPlugin = new QuartzPlugin("job.properties");
		me.add(configPlugin);
	}
	
	/**
	 * 配置全局拦截器
	 */
	public void configInterceptor(Interceptors me) {
		me.add(new AuthInterceptor());
//		me.add(new AuthJsonInterceptor());
	}
	
	/**
	 * 配置处理器
	 */
	public void configHandler(Handlers me) {
	}

	/**
	 * 建议使用 JFinal 手册推荐的方式启动项目
	 * 运行此 main 方法可以启动项目，此main方法可以放置在任意的Class类定义中，不一定要放于此
	 */
	public static void main(String[] args) {
		JFinal.start("src/main/webapp", 80, "/", 5);
	}
}
