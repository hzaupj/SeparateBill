package com.hzaupj.pindan.vo;

import java.util.Date;

/**
 * @Description: MX_N
 * @author: pengjian_91
 * @date: 2015/4/7
 * @version: V1.0
 */
public class Order {
    private String id;
    private Food food;
    private String pindanId;
    private long userId;
    private String remark;
    private double disPrice;
    private int type; // 1-正常订单 2-求拼订单
    private int difFloorNum; // 取餐楼层正负数
    private int maxCommissionRate = 3; //  愿意支付的最大佣金比例
    private Date startDate; // 发起时间
    private double avgSendPrice; // 均摊配送费
    private String requestShareRemark; // 求拼备注
	private int isPay; // 是否付款

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPindanId() {
        return pindanId;
    }

    public void setPindanId(String pindanId) {
        this.pindanId = pindanId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public Food getFood() {
        return food;
    }

    public void setFood(Food food) {
        this.food = food;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public double getDisPrice() {
        return disPrice;
    }

    public void setDisPrice(double disPrice) {
        this.disPrice = disPrice;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getDifFloorNum() {
        return difFloorNum;
    }

    public void setDifFloorNum(int difFloorNum) {
        this.difFloorNum = difFloorNum;
    }

    public int getMaxCommissionRate() {
        return maxCommissionRate;
    }

    public void setMaxCommissionRate(int maxCommissionRate) {
        this.maxCommissionRate = maxCommissionRate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public double getAvgSendPrice() {
        return avgSendPrice;
    }

    public void setAvgSendPrice(double avgSendPrice) {
        this.avgSendPrice = avgSendPrice;
    }

    public String getRequestShareRemark() {
        return requestShareRemark;
    }

    public void setRequestShareRemark(String requestShareRemark) {
        this.requestShareRemark = requestShareRemark;
    }

	public int getIsPay() {
		return isPay;
	}

	public void setIsPay(int isPay) {
		this.isPay = isPay;
	}
}
