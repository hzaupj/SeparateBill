package com.hzaupj.pindan.task;

import com.hzaupj.pindan.service.MainService;
import com.hzaupj.pindan.vo.Order;
import org.joda.time.DateTime;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * @Description: MX_N
 * @author: pengjian_91
 * @date: 2015/4/27
 * @version: V1.0
 */
public class CleanReqShareList implements Job {
	private static Logger logger = LoggerFactory.getLogger(CleanReqShareList.class);

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
		logger.info("清空求拼列表 start " + DateTime.now().toString("yyyy-MM-dd HH:mm:ss"));
        Map<String, Order> reqShareOrderMap = MainService.getReqShareOrderMap();
        reqShareOrderMap.clear();
		logger.info("清空求拼列表 end " + DateTime.now().toString("yyyy-MM-dd HH:mm:ss"));
    }
}
