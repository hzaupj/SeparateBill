package com.hzaupj.pindan.json;

/**
 * @Description: MX_N
 * @author: pengjian_91
 * @date: 2015/5/4
 * @version: V1.0
 */
public class JFoodForElm {
    private int id;
    private String name;
    private double price;
    private String img;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }
}
