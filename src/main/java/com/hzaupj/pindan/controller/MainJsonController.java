package com.hzaupj.pindan.controller;

import com.hzaupj.pindan.MainConfig;
import com.hzaupj.pindan.OrderPlatform;
import com.hzaupj.pindan.dao.UserDao;
import com.hzaupj.pindan.service.MainService;
import com.hzaupj.pindan.vo.*;
import com.jfinal.plugin.activerecord.Record;
import jodd.util.StringUtil;

import java.util.*;

/**
 * MainController
 */
public class MainJsonController extends BaseJsonController {

    /**
     * 餐厅列表
     * @throws Exception
     */
    public void hallList() throws Exception {
        int opf = getParaToInt(0);

        MainService.hallList(opf);

        Map<String, Object> result = new HashMap<String, Object>();
        result.put("platform", new PlatForm(OrderPlatform.getOpf(opf)));
        result.put("hallList", new ArrayList(MainService.getHallTable().row(opf).values()));

        renderOkJson(result);
    }

    /**
     * 食物列表
     *
     * @throws Exception
     */
    public void foodList() throws Exception {
        int platformId = getParaToInt(0);
        String hallId = getPara("hallId");

        MainService.foodList(platformId, hallId);

        Hall hall = MainService.getHallTable().row(platformId).get(hallId);
        Collection<Food> list = MainService.getFoodTable().row(hallId).values();

        Map<String, Object> result = new HashMap<String, Object>();
        result.put("platform", new PlatForm(OrderPlatform.getOpf(platformId)));
        result.put("foodList", new ArrayList(list));
        result.put("hall", hall);

        renderOkJson(result);
    }

    /**
     * 拼单列表
     *
     * @throws Exception
     */
    public void pindanList() throws Exception {
        Integer status = getParaToInt(0);
        if(status == null){
            status = -1;
        }

		long loginUserId = MainConfig.getLoginUser().getLong("id");
		// 我发起
		Map<String, PinDan> myStartPindanMap = new HashMap<String, PinDan>();
		// 我参与
		Map<String, PinDan> myPartPindanMap = new HashMap<String, PinDan>();

        Map<String, Record> userMap = new HashMap<String, Record>();
        Map<String, Integer[]> orderInfoMap = new HashMap<String, Integer[]>();
        Iterator<String> it = MainService.getPindanMap().keySet().iterator();
        while (it.hasNext()){
            String pindanId = it.next();
            PinDan pindan = MainService.getPindanMap().get(pindanId);
            int orderNum = MainService.getOrderNum(pindanId);
            int orderMoney = MainService.getOrderMoney(pindanId);
            orderInfoMap.put(pindanId, new Integer[]{orderNum, orderMoney});

            Record user = UserDao.inquireById(pindan.getUserId());
            if(user != null){
                userMap.put(pindanId, user);
            }else{
                userMap.put(pindanId, new Record());
            }

			// 获取我发起的拼单
			if(pindan.getUserId() == loginUserId){
				myStartPindanMap.put(pindanId, pindan);
			}
			// 获取我参与的拼单（包含我发起）
			if(MainService.isPartOfPindan(pindanId, loginUserId)){
				myPartPindanMap.put(pindanId, pindan);
			}
        }

        Map<String, PinDan> orderPindanMap = MainService.filterPindanMap(status);
        orderPindanMap = MainService.orderPindanMap(orderPindanMap);

        Map<String, Object> result = new HashMap<String, Object>();
        result.put("orderInfoMap", orderInfoMap);
        result.put("userMap", userMap);
        result.put("pindanMap", orderPindanMap);
		result.put("myStartPindanMap", myStartPindanMap);
		result.put("myPartPindanMap", myPartPindanMap);

        renderOkJson(result);
    }

    /**
     * 新增一个拼单
     *
     * @throws Exception
     */
    public void addPindan() throws Exception {
        int platformId = getParaToInt(0);
        String hallId = getPara("hallId");
        String code = getPara("code");

        if(MainService.getHallTable().row(platformId).isEmpty()){
            renderErrorJson("餐厅列表刚才正被刷新，请重试！");
            return;
        }

        boolean isTrueCode = false;
        if(!StringUtil.isEmpty(code)){
            if(code.length() != 4 || !MainService.isNumeric(code)){
                renderErrorJson("请输入正确的4位数字作为拼单口令！");
                return;
            }
            isTrueCode = true;
        }
        MainService.addPindan(platformId, hallId, code, isTrueCode);
		renderOkJson("新增成功");
    }

    /**
     * 点餐
     *
     * @throws Exception
     */
    public void order() throws Exception {
        String pindanId = getPara("pindanId");
        String hallId = getPara("hallId");
        String foodId = getPara("foodId");

        Food selectedFood = MainService.getFoodTable().get(hallId, foodId);
        if (selectedFood == null) {
            renderErrorJson("亲，找不到这个餐！");
            return;
        }

        PinDan pindan = MainService.getPindanMap().get(pindanId);
        if(pindan == null){
            renderErrorJson("拼单不存在");
            return;
        }
        if(pindan.getStatus() == 0){
            renderErrorJson("已停止点餐！");
            return;
        }

        MainService.order(pindan, hallId, foodId);

        renderOkJson("点餐成功");
    }

	/**
	 * 添加一个求拼
	 *
	 * @throws Exception
	 */
	public void addRequestShareOrder() throws Exception {
		String hallId = getPara("hallId");
		String foodId = getPara("foodId");
		Integer maxCommissionRate = getParaToInt("mcr",3);
		String remark = getPara("remark");

		Food selectedFood = MainService.getFoodTable().get(hallId, foodId);
		if (selectedFood == null) {
			renderErrorJson("亲，找不到这个餐！");
			return;
		}

		MainService.addRequestShareOrder(hallId, foodId, maxCommissionRate, remark);

		renderOkJson("新增求拼信息成功");
	}

	/**
	 * 加入拼单
	 */
	public void joinPindan() throws Exception{
		String orderId = getPara("orderId");
		String pindanId = getPara("pindanId");

		Order reqShareOrder = MainService.getReqShareOrderMap().get(orderId);
		PinDan pinDan = MainService.getPindanMap().get(pindanId);
		MainService.joinPindan(pinDan, reqShareOrder);

		renderOkJson("加入成功");
	}


	/**
	 * 删除求拼
	 * @throws Exception
	 */
	public void deleteReq() throws Exception {
		String orderId = getPara("orderId");
		Order order = MainService.getReqShareOrderMap().get(orderId);
		if(order == null){
			renderErrorJson("订单不存在");
			return;
		}

		MainService.deleteReq(order);

		renderOkJson("删除成功");
	}

	/**
	 * 新增拆单
	 * @throws Exception
	 */
	public void addSB() throws Exception {
		String pindanId = getPara("pindanId");
		String sbId = MainService.addSB(pindanId);
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("sbId", sbId);
		renderOkJson(result);
	}

	/**
	 * 删除拆单
	 * @throws Exception
	 */
	public void deleteSB() throws Exception {
		String pindanId = getPara("pindanId");
		String sbId = getPara("sbId");
		SeparateBill sb = MainService.getSB(pindanId, sbId);
		if(sb != null){
			if(sb.getOrderList() != null && sb.getOrderList().size()>0){
				renderErrorJson("只能删除空单！该单下还有订单，请移除后再删除!");
				return;
			}
		}
		MainService.deleteSB(pindanId, sbId);
		renderOkJson("删除成功");
	}

	/**
	 * 打开自动拆单
	 * @throws Exception
	 */
	public void openAuto() throws Exception {
		String pindanId = getPara("pindanId");
		PinDan pinDan = MainService.getPindanMap().get(pindanId);
		if(pinDan == null){
			renderErrorJson("找不到这个拼单！");
			return;
		}

		MainService.openAuto(pinDan);
		renderOkJson("切换成功");
	}

	/**
	 * 关闭自动拆单
	 * @throws Exception
	 */
	public void closeAuto() throws Exception {
		String pindanId = getPara("pindanId");
		PinDan pinDan = MainService.getPindanMap().get(pindanId);
		if(pinDan == null){
			renderErrorJson("找不到这个拼单！");
			return;
		}

		MainService.closeAuto(pinDan);
		renderOkJson("切换成功");
	}

	/**
	 * 移动订单
	 * @throws Exception
	 */
	public void moveTo() throws Exception {
		String pindanId = getPara("pindanId");
		String orderId = getPara("orderId");
		String fromSbId = getPara("fromSbId");
		String toSbId = getPara("toSbId");

		PinDan pinDan = MainService.getPindanMap().get(pindanId);
		if(pinDan == null){
			renderErrorJson("找不到这个拼单！");
			return;
		}

		Order order = MainService.getOrderMap().get(orderId);
		if(order == null){
			renderErrorJson("订单不存在");
			return;
		}

		MainService.moveTo(pinDan, fromSbId, toSbId, order);
		renderOkJson();
	}

	/**
	 * 求拼列表
	 *
	 * @throws Exception
	 */
	public void requestShareList() throws Exception {
		List<Order> orderList = MainService.requestShareOrderList();
		Map<String, Record> userMap = new HashMap<String, Record>();
		Map<String, Hall> hallMap = new HashMap<String, Hall>();
		if(orderList != null) {
			for (Order order : orderList) {
				// 设置用户Map
				Record user = UserDao.inquireById(order.getUserId());
				if (user != null) {
					userMap.put(order.getId(), user);
				} else {
					userMap.put(order.getId(), new Record());
				}

				// 设置餐厅Map
				hallMap.put(order.getId(),
						MainService.getHallTable()
								.get(order.getFood().getPlatform().getCode(), order.getFood().getHallId()));
			}
		}
		// 求拼列表按照佣金金额排序
		orderList = MainService.sortReqShareOrderList(orderList);

		Map<String, Object> result = new HashMap<String, Object>();
		// 获取我的拼单列表
		result.put("myPindanList", MainService.getMyPindanList(MainConfig.getLoginUser().getLong("id")));
		result.put("orderList", orderList);
		result.put("userMap", userMap);
		result.put("hallMap", hallMap);

		renderOkJson(result);
	}

	/**
	 * 修改备注
	 * @throws Exception
	 */
	public void updateRemark() throws Exception {
		String orderId = getPara("orderId");
		String remark = getPara("remark");
		Order order = MainService.getOrderMap().get(orderId);
		if(order == null){
			renderErrorJson("订单不存在");
			return;
		}
		order.setRemark(remark);

		renderOkJson("ok");
	}

	/**
	 * 修改求拼备注
	 * @throws Exception
	 */
	public void updateRequestShareRemark() throws Exception {
		String orderId = getPara("orderId");
		String remark = getPara("remark");
		Order order = MainService.getReqShareOrderMap().get(orderId);
		if(order == null){
			renderErrorJson("订单不存在");
			return;
		}
		order.setRequestShareRemark(remark);

		renderOkJson("ok");
	}

	/**
	 * 修改转账状态
	 * @throws Exception
	 */
	public void updateIsPay() throws Exception {
		String orderId = getPara("orderId");
		Integer isPay = getParaToInt("isPay");
		Order order = MainService.getOrderMap().get(orderId);
		if(order == null){
			renderErrorJson("订单不存在");
			return;
		}
		order.setIsPay(isPay);

		renderOkJson("ok");
	}

    /**
     * 拼单详情
     *
     * @throws Exception
     */
    public void report() throws Exception {
        String pindanId = getPara("pindanId");
        int platformId = getParaToInt(0);
        String hallId = getPara("hallId");
        if (MainService.getOrderMap() == null) {
            renderErrorJson("没有任何人点餐！");
            return;
        }

        PinDan pindan = MainService.getPindanMap().get(pindanId);
        if(pindan == null){
            renderErrorJson("拼单已不存在！");
            return;
        }

        Hall hall = MainService.getHallTable().row(platformId).get(hallId);
        List<Order> orderList = MainService.getOrderList(pindan.getId(), MainService.getOrderMap());

        Map<String, SeparateBill> reportMap = MainService.report(pindan, hall, orderList);

        Record owner = UserDao.inquireById(pindan.getUserId());
        Map<Long, Record> userMap = MainService.getUserMap(orderList);

        Map<String, Object> result = new HashMap<String, Object>();
        result.put("userMap", userMap);
        result.put("map", reportMap);
        result.put("hall", hall);
        result.put("owner", owner);
        result.put("pindan", pindan);

        renderOkJson(result);
    }

    /**
     * 删除订餐
     * @throws Exception
     */
    public void delete() throws Exception {
        String orderId = getPara("orderId");
        Order order = MainService.getOrderMap().get(orderId);
        if(order == null){
            renderErrorJson("订单不存在");
            return;
        }
        PinDan pindan = MainService.getPindanMap().get(order.getPindanId());
        if(pindan == null){
            renderErrorJson("拼单不存在");
            return;
        }
        if(pindan.getStatus() == 0){
            renderErrorJson("已停止点餐，不允许删除！");
            return;
        }

        MainService.delete(order);

        renderOkJson("删除成功");
    }

    /**
     * 删除拼单
     * @throws Exception
     */
    public void deletePindan() throws Exception {
        String pindanId = getPara("pindanId");
        PinDan pindan = MainService.getPindanMap().get(pindanId);
        if(pindan == null){
            renderErrorJson("拼单已不存在！");
            return;
        }

        if(pindan.getStatus() == 1){
            renderErrorJson("请先停止点餐再删除该拼单！");
            return;
        }

        // 删除拼单
        MainService.deletePindan(pindanId);

        renderOkJson("删除成功");
    }

    /**
     * 启用拼单
     * @throws Exception
     */
    public void start() throws Exception {
        String pindanId = getPara("pindanId");
        PinDan pinDan = MainService.getPindanMap().get(pindanId);
        if(pinDan == null){
            renderErrorJson("找不到这个拼单！");
            return;
        }

        MainService.start(pinDan);

        renderOkJson("启用成功");
    }

    /**
     * 停止拼单
     * @throws Exception
     */
    public void stop() throws Exception {
        String pindanId = getPara("pindanId");
        PinDan pinDan = MainService.getPindanMap().get(pindanId);
        if(pinDan == null){
            renderErrorJson("找不到这个拼单！");
            return;
        }

        MainService.stop(pinDan);
        renderOkJson("停止成功");
    }

    /**
     * 修改拼单模式
     * @throws Exception
     */
    public void changePindanMode() throws Exception {
        String pindanId = getPara("pindanId");
        PinDan pinDan = MainService.getPindanMap().get(pindanId);
        if(pinDan == null){
            renderErrorJson("找不到这个拼单！");
            return;
        }

        MainService.changePindanMode(pinDan);
        renderOkJson("模式切换成功");
    }

    /**
     * 设置自动结单金额
     * @throws Exception
     */
    public void setAutoStopMoney() throws Exception {
        String pindanId = getPara("pindanId");
        String autoMoney = getPara("autoMoney");
        PinDan pinDan = MainService.getPindanMap().get(pindanId);
        if(pinDan == null){
            renderErrorJson("找不到这个拼单！");
            return;
        }

        if(autoMoney == null || !MainService.isNumeric(autoMoney)){
            renderErrorJson("请输入正确的整数金额(元)！(输入0代表取消自动结单功能)");
            return;
        }

        MainService.setAutoStopMoney(pinDan, new Integer(autoMoney));
        renderOkJson("设置成功");
    }
}