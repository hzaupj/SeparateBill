package com.hzaupj.test;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.hzaupj.pindan.vo.Hall;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @Description: MX_N
 * @author: pengjian_91
 * @date: 2015/4/20
 * @version: V1.0
 */
public class TestElm {

    @Test
    public void testHall() throws Exception {
        String url = "http://ele.me/restapi/v1/restaurants?extras[]=food_activity&extras[]=restaurant_activity&extras[]=certification&fields[]=id&fields[]=name&fields[]=phone&fields[]=promotion_info&fields[]=name_for_url&fields[]=flavors&fields[]=is_time_ensure&fields[]=is_premium&fields[]=image_path&fields[]=rating&fields[]=is_free_delivery&fields[]=minimum_order_amount&fields[]=order_lead_time&fields[]=is_support_invoice&fields[]=is_new&fields[]=is_third_party_delivery&fields[]=is_in_book_time&fields[]=rating_count&fields[]=address&fields[]=month_sales&fields[]=delivery_fee&fields[]=minimum_free_delivery_amount&fields[]=minimum_order_description&fields[]=minimum_invoice_amount&fields[]=opening_hours&fields[]=is_online_payment&fields[]=status&fields[]=supports&geohash=wssu3x04c6b&is_premium=0&offset=0&type=geohash";
        HttpGet httpGet = new HttpGet(url);
        HttpClient client = HttpClients.createDefault();
        HttpResponse response = client.execute(httpGet);
        HttpEntity entity = response.getEntity();
        JSONArray array = JSONObject.parseArray(EntityUtils.toString(entity));
        for(Object object : array){
            JSONObject jsonObject = (JSONObject)object;
            Hall hall = new Hall();
            hall.setId(jsonObject.getString("name_for_url"));
            hall.setName(jsonObject.getString("name"));
            hall.setPicUrl("http://fuss10.elemecdn.com" + jsonObject.getString("image_path"));
            hall.setBulletinInfo(jsonObject.getString("promotion_info"));
            hall.setEnable(jsonObject.getBoolean("is_in_book_time") ? 1 : 0);
            hall.setDisList(getDisList(jsonObject));
            System.out.println(hall);
        }
    }

    private List<String[]> getDisList(JSONObject jsonObject) {
        List<String[]> resultList = new ArrayList<String[]>();
        JSONArray array = jsonObject.getJSONArray("restaurant_activity");
        if(array == null || array.isEmpty()){
            return resultList;
        }

        for(Object o : array){
            JSONObject jo = (JSONObject)o;
            int type = jo.getInteger("type");
            if(type == 102){
                String attr = jo.getString("attribute");
                Map<String, Map<String, Integer>> disMap = JSONObject.parseObject(attr, Map.class);
                Iterator<String> it = disMap.keySet().iterator();
                while (it.hasNext()){
                    String fullPic = it.next();
                    int disPic = disMap.get(fullPic).get("1");
                    resultList.add(new String[]{fullPic, String.valueOf(disPic)});
                }
            }
        }
        return resultList;
    }

    @Test
    public void testFoodMeituan() throws Exception {
        String jsonString = "{id:17373891,name:米饭,price:2,origin_price:2,minCount:1,onSale:1,sku:[{id:17350316,name:asdfasd,price:2,origin_price:2,minCount:1,stock:0,isSellOut:0}]}";

        JSONObject obj = JSONObject.parseObject(jsonString);
        System.out.println(obj);

    }

}
