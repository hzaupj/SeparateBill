package com.hzaupj.pindan.vo;

import com.hzaupj.pindan.OrderPlatform;

/**
 * @Description: MX_N
 * @author: pengjian_91
 * @date: 2015/5/8
 * @version: V1.0
 */
public class PlatForm {

    private int code;
    private String name;
    private String url;
    private String picUrl;

    public PlatForm(OrderPlatform orderPlatform) {
        this.code = orderPlatform.getCode();
        this.name = orderPlatform.getName();
        this.url = orderPlatform.getUrl();
        this.picUrl = orderPlatform.getPicUrl();
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }
}
