package com.hzaupj.pindan.json;

/**
 * @Description: MX_N
 * @author: pengjian_91
 * @date: 2015/4/3
 * @version: V1.0
 */
public class JFood {
    private int sku; // foodId
    private String[] attrs = new String[]{};
    private int num;

    public int getSku() {
        return sku;
    }

    public void setSku(int sku) {
        this.sku = sku;
    }

    public String[] getAttrs() {
        return attrs;
    }

    public void setAttrs(String[] attrs) {
        this.attrs = attrs;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }
}
