'user strict';
var shopModule = angular.module('shopModule', ['shopControllers', 'spbServices', 'ui.bootstrap']);
var shopControllers = angular.module('shopControllers', ['ngResource']);

shopControllers.controller('platformCtrl', ['$scope', '$localStorage', 'Api',
    function($scope, $localStorage, Api) {
        $scope.platformList = [] ;
        Api.platform.list.query({})
        .$promise.then(
            function(data){
                $scope.platformList = data;
            },
            function(data) {
                console.log(data.status,"请求出错");
            }
        );
        $scope.getList = function(id){
            $localStorage.platformId = id;
        }
    }
]);

shopControllers.controller('shopListCtrl', ['$scope', '$state', '$stateParams', 'SweetAlert', 'Api',
    function($scope, $state, $stateParams, SweetAlert, Api) {
        $scope.shops = [];
        $scope.platformItem = [];

        Api.shop.list.query({platformId: $stateParams.platformId})
        .$promise.then(
            function(data) {
                if (data.result) {
                    $scope.enable = "1";
                    $scope.shops = data.result.hallList;
                    $scope.platformItem = $scope.shops[0].platform;
                }else if(data.flag === "UNLOGIN"){
                    $state.go("login");
                };
            },
            function(data) {
                console.log(data,"请求出错");
            }
        );

        //点餐
        $scope.addOrder = function(platformId,hallId,pindanId,hasPublic,code){
            swal({   
                title: "发起拼单",   
                text: "其他人加入此拼单是否需要口令?",   
                type: "input",   
                inputPlaceholder: "请输入拼单口令",
                showCancelButton: true,   
                confirmButtonColor: "#FF8585",   
                confirmButtonText: "需要",   
                cancelButtonText: "不需要",   
                closeOnConfirm: false,
                closeOnCancel: false,
            }, function(inputValue){
                if(inputValue === false){
                    Api.order.add.query({
                        platformId: platformId, 
                        hallId: hallId
                    })
                    .$promise.then(
                        function(data) {
                            if(data.flag == "SUCCESS"){
                                swal({
                                    title:"新增拼单成功！",
                                    text:"将跳转到拼单页面",
                                    type:"success"
                                },function(){
                                    $state.go('order');
                                    // $state.go('book',{platformId: platformId, hallId: hallId, pindanId: pindanId, hasPublic: hasPublic});
                                });
                            }
                        },
                        function(data) {
                            console.log(data,"请求出错");
                        }
                    );
                }else if (inputValue === "" || inputValue.length !== 4 || isNaN(inputValue)) {  
                    swal.showInputError("请输入4位数字拼单口令");     
                }else if(inputValue !== ""){
                    Api.order.add.query({
                        platformId: platformId, 
                        hallId: hallId,
                        code:inputValue
                    })
                    .$promise.then(
                        function(data) {
                            if(data.flag == "SUCCESS"){
                                swal({
                                    title:"新增私密拼单成功！",
                                    text:"将跳转到拼单页面",
                                    type:"success"
                                },function(){
                                    $state.go('order');
                                });
                            }
                        },
                        function(data) {
                            console.log(data,"请求出错");
                        }
                    );
                }
            });
        };



    }
]);

shopControllers.controller('shopDetailCtrl', ['$scope', '$state', '$stateParams', 'Api', '$modal', 
    function($scope, $state, $stateParams, Api, $modal) {
        $scope.shop = [];
        $scope.hasCurUser = false;
        $scope.hasOwner = false;
        Api.shop.detail.query({
            platformId:$stateParams.platformId,
            hallId:$stateParams.hallId
        })
        .$promise.then(
            function(data) {
                if (data.flag == "SUCCESS") {
                   $scope.shop = data.result;
                }else if(data.flag == "UNLOGIN"){
                     $state.go('login')
                };
            },
            function(data) {
                console.log(data,"请求出错");
            }
        );


        $scope.open = function(size,item) {
            $scope.food = item;
            $scope.food.commPercent = 3;
            $scope.food.remark = "";
            var modalInstance = $modal.open({
                animation: $scope.animationsEnabled,
                templateUrl: 'reqModal.html',
                controller: 'reqModalCtrl1',
                size: size,
                resolve: {
                    food: function() {
                        return $scope.food;
                    }
                }
            });

            modalInstance.result.then(function(food) {
                Api.reqorder.add.query({
                    hallId: food.hallId,
                    foodId: food.id,
                    remark: food.remark,
                    mcr: food.commPercent
                })
                .$promise.then(
                    function(data) {
                        if(data.flag == "SUCCESS"){
                            swal({
                                title:"新增求拼单成功",
                                text:"将跳转到求拼列表页面",
                                type:"success"
                            },function(){
                                $state.go('reqorder');
                            });
                        }else{
                            swal({
                                title:"新增求拼单失败",
                                text: data.msg,
                                type:"error"
                            });
                        }
                    },
                    function(data) {
                        console.log(data,"请求出错");
                    }
                );
            }, function() {
                //取消
            });
        };


        //判断当前用户是否在订单用户中
        angular.forEach($scope.user, function(item,index){
            if(item.columns.account == curUser){
                $scope.hasCurUser = true;
                $scope.curUserInfo["account"] = item.columns.account;
                $scope.curUserInfo["id"] = item.columns.id;
            }
        });


    }
]);



shopControllers.controller('reqModalCtrl1', function ($scope, $modalInstance, food) {

    $scope.food = food;
  // $scope.selected = {
  //   item: $scope.items[0]
  // };
    $scope.ok = function () {
        $modalInstance.close($scope.food);
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
});

