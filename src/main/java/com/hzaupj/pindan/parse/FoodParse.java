package com.hzaupj.pindan.parse;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.hzaupj.pindan.OrderPlatform;
import com.hzaupj.pindan.json.JFoodCategForElm;
import com.hzaupj.pindan.json.JFoodForElm;
import com.hzaupj.pindan.vo.Food;
import com.hzaupj.pindan.vo.PlatForm;
import jodd.util.StringUtil;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by pengjian_91 on 2015/3/10.
 */
public class FoodParse {

    public static List<Food> getFoodList(OrderPlatform opf, String hallId) throws IOException {
        List<Food> resultList = new ArrayList<Food>();
        switch (opf){
        case MEITUAN:
            resultList = getFoodListForMeituan(hallId);
            break;
        case ELM:
            resultList = getFoodListForElm(hallId);
            break;
		case BAIDU:
            resultList = getFoodListForBaidu(hallId);
            break;
        default:
            break;
        }
        return resultList;
    }

    private static List<Food> getFoodListForMeituan(String hallId) throws IOException {
        String url = String.format("http://waimai.meituan.com/restaurant/%s", hallId);

        List<Food> foods = new ArrayList<Food>();
        Document doc = Jsoup.connect(url).timeout(5000)
				.userAgent(
						"Mozilla/5.0 (Windows NT 6.1; WOW64)" + UUID.randomUUID().toString())
				.cookie("REMOVE_ACT_ALERT", "1")
				.cookie("w_geoid", HallParse.MT_PLACECODE).get();
        Elements eles = doc.select("div[class~=(pic-food|text-food)]");
        for(int i=0; i<eles.size(); i++){
            String picUrl = getPicUrlForMeiTuan(eles.get(i));
            List<String[]> foodInfosList = getFoodInfoForMeiTuan(eles.get(i));
            for(String[] foodInfos : foodInfosList){
                Food food = new Food();
                food.setHallId(hallId);
                food.setPlatform(new PlatForm(OrderPlatform.MEITUAN));
                food.setName(foodInfos[0]);
                double price = Double.valueOf(foodInfos[1].replace(",", "")).doubleValue()*10.0;
                food.setPrice(Double.valueOf(price).intValue());
                food.setId(foodInfos[2]);
                food.setPicUrl(picUrl);
                if(Integer.valueOf(foodInfos[3])==0){
                    food.setOnSale(1);
                }
                foods.add(food);
            }
        }
        return foods;
    }

    private static List<Food> getFoodListForElm(String hallId) throws IOException {
        String url = String.format("http://r.ele.me/%s", hallId);

        List<Food> foods = new ArrayList<Food>();
        Document doc = Jsoup.connect(url).timeout(5000)
				.userAgent(
						"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.130 "
								+ "Safari/537.36")
                .cookie("uuid", "08a30f1c8432b86abea7.1439550808.0.0.0")
                .cookie("oc",
						"uzitvqnothY-K707Abav6-X2pMjglf-cEa2XG_HzPlay2CH_AU356stNIWl6rSadG8LYdMtz3AoQ7s3hlSp6Vko8sukx8X8Pp"
								+ "-qYaCD4rJ0ZsiF5JSq0JMjdBZ1B0T-bcB_XKNrESrBQILmJ0LyRa9LWMGiWGk3_uClg8NOmR0Y")
                .cookie("w_visitid", "f1c01f2c-bda3-4204-8e9a-af4cd67129b3")
                .cookie("JSESSIONID", "11c931nvbsw6n1inhltaxeqg42")
				.get();
		Elements eles = doc.select("script[type*=text/javascript]");
        List<JFoodForElm> allList = new ArrayList<JFoodForElm>();
        for (int i=0; i<eles.size(); i++){
            String menusJsonString = getMenusJsonString(eles).replace("\\\"", "\"");

            if(StringUtil.isNotEmpty(menusJsonString)){
                List<JFoodCategForElm> jsonArray = JSONArray.parseArray(menusJsonString, JFoodCategForElm.class);
                for(JFoodCategForElm categForElm : jsonArray){
                    if(categForElm.getFoods().getWith_image() != null && !categForElm.getFoods().getWith_image().isEmpty()){
                        allList.addAll(categForElm.getFoods().getWith_image());
                    }
                    if(categForElm.getFoods().getWithout_image() != null && !categForElm.getFoods().getWithout_image().isEmpty()){
                        allList.addAll(categForElm.getFoods().getWithout_image());
                    }
                }
            }
        }

        addFoodList(hallId, foods, allList);

        return foods;
    }

	private static List<Food> getFoodListForBaidu(String hallId) throws IOException {
		List<Food> foodList = new ArrayList<Food>();
		try {
			String url = String.format("http://waimai.baidu.com/waimai/shop/%s", hallId);
			Document doc = Jsoup.connect(url).get();

			Food food = null;
			String[] dataArray = null;
			for (Element ele : doc.select("section.menu-list li.list-item")) {
				food = new Food();
				dataArray = ele.attr("data").split("\\$");

				food.setId(dataArray[0]);
				food.setName(dataArray[1]);
				Double price = Double.valueOf(dataArray[2].replaceAll(",", ""))*10.0;
				food.setPrice(price.intValue());

				// 图片的获取
				String picUrl = ele.select("figure.headimg div").attr("style");
				picUrl = StringUtils.substringBetween(picUrl, "url(", ")");
				food.setPicUrl(picUrl);

				food.setOnSale(1);
				food.setHallId(hallId);
				food.setPlatform(new PlatForm(OrderPlatform.BAIDU));

				foodList.add(food);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

		return foodList;
	}

    private static void addFoodList(String hallId, List<Food> foods, List<JFoodForElm> allList) {
        for(JFoodForElm jFoodForElm : allList){
            Food food = new Food();
            food.setId(String.valueOf(jFoodForElm.getId()));
            food.setName(convert(jFoodForElm.getName().replace("\\\\", "\\")));
            if(!StringUtil.isEmpty(jFoodForElm.getImg())){
                food.setPicUrl("http://fuss10.elemecdn.com" + jFoodForElm.getImg());
            }
            food.setPrice(Double.valueOf(jFoodForElm.getPrice()*10).intValue());
            food.setOnSale(1);
            food.setHallId(hallId);
            food.setPlatform(new PlatForm(OrderPlatform.ELM));
            foods.add(food);
        }
    }

    private static String getMenusJsonString(Elements eles) {
        for (int i=0; i<eles.size(); i++){
            String content = eles.get(i).data();
            String patternStr = "(\\[.*\\])";
            Pattern pattern = Pattern.compile(patternStr);
            Matcher m = pattern.matcher(content);
            if(m.find()){
                return m.group(1);
            }
        }
        return null;
    }

    private static boolean getIsOpenForElm(Element element) {
        Elements elements = element.select("span[class=status]");
        for(Element el : elements){
            if("已售完".equals(el.text())){
                return false;
            }
        }
        return true;
    }

    private static String getIdForElm(Element element) {
        String id = element.attr("id");
        String patternStr = "food_view_(\\d*)";
        Pattern pattern = Pattern.compile(patternStr);
        Matcher m = pattern.matcher(id);
        if(m.find()){
            return m.group(1);
        }
        return null;
    }

    private static String getPriceForElm(Element element) {
        Elements eles = element.select("span[class*=price]");
        for(Element el : eles){
            return el.text().replaceAll("\\n", "").replaceAll("\\r", "");
        }
        return "0";
    }

    private static String getFoodNameForElm(Element element) {
        Elements eles = element.select("a[class*=food_name]");
        for(Element el : eles){
            return el.attr("title");
        }
        return null;
    }

    private static String getPicUrlForElm(Element element) throws IOException {
        Elements eles = element.select("img[class=rst-d-img]");
        String patternStr = "(^http:\\S*) ";
        for(Element el : eles){
            String srcset = el.attr("srcset");
            Pattern pattern = Pattern.compile(patternStr);
            Matcher m = pattern.matcher(srcset);
            if(m.find()){
                return m.group(1);
            }
        }
        return null;
    }

    private static String getPicUrlForMeiTuan(Element element) throws IOException {
        Elements eles = element.select("div.avatar");
        String patternStr = "data-src=\"(\\S*)\"";
        for(Element el : eles){
            String els = el.toString();
            Pattern pattern = Pattern.compile(patternStr);
            Matcher m = pattern.matcher(els);
            if(m.find()){
               return m.group(1);
            }
        }
        return null;
    }

    private static List<String[]> getFoodInfoForMeiTuan(Element element){
        List<String[]> resultlist = new ArrayList<String[]>();

        Elements eles = element.select("script").select("[type=text/template]").select("[id~=foodcontext*]");
        for(Element el : eles){
            String temp = el.toString()
                    .replaceAll(" ", "")
                    .replaceAll("\\n", "").replaceAll("\\r", "");
            JSONObject foodJson = null;
            Pattern pattern = Pattern.compile("\\{.*\\}");
            Matcher m = pattern.matcher(temp);
            if(m.find()){
                foodJson = JSONObject.parseObject(m.group(0));
            }
            if(foodJson == null){
                resultlist.add(new String[4]);
                return resultlist;
            }

            JSONArray foodArray = foodJson.getJSONArray("sku");
            if(foodArray == null){
                resultlist.add(new String[4]);
                return resultlist;
            }
            for (Object sku : foodArray){
                String[] foodS = new String[4];
                JSONObject skuObject = (JSONObject)sku;
                String subName = StringUtil.isNotEmpty(skuObject.getString("name")) ? "(" + skuObject.getString("name") + ")" : "";
                foodS[0] = foodJson.getString("name") + subName;
                foodS[1] = skuObject.getString("price");
                foodS[2] = skuObject.getString("id");
                foodS[3] = skuObject.getString("isSellOut");
                resultlist.add(foodS);
            }
        }
        return resultlist;
    }

	public static String convert(String utfString){
		StringBuilder sb = new StringBuilder();
		int i = -1;
		int pos = 0;

		while((i=utfString.indexOf("\\u", pos)) != -1){
			sb.append(utfString.substring(pos, i));
			if(i+5 < utfString.length()){
				pos = i+6;
				sb.append((char)Integer.parseInt(utfString.substring(i+2, i+6), 16));
			}
		}

		return sb.toString();
	}
}
