package com.hzaupj.pindan;

/**
 * @Description: MX_N
 * @author: pengjian_91
 * @date: 2015/4/20
 * @version: V1.0
 */
public enum OrderPlatform {

    MEITUAN(1,"美团外卖",
            "http://waimai.meituan.com/home/wssu98hv34jd",
            "https://ss0.bdstatic.com/-0U0bnSm1A5BphGlnYG/tam-ogel/bc0d81cae2892fdfe615a8d0642d5e40_121_121.jpg"),
    ELM(2,"饿了么",
            "http://ele.me/restapi/v1/restaurants?extras[]=food_activity&extras[]=restaurant_activity&extras[]=certification&fields[]=id&fields[]=name&fields[]=phone&fields[]=promotion_info&fields[]=name_for_url&fields[]=flavors&fields[]=is_time_ensure&fields[]=is_premium&fields[]=image_path&fields[]=rating&fields[]=is_free_delivery&fields[]=minimum_order_amount&fields[]=order_lead_time&fields[]=is_support_invoice&fields[]=is_new&fields[]=is_third_party_delivery&fields[]=is_in_book_time&fields[]=rating_count&fields[]=address&fields[]=month_sales&fields[]=delivery_fee&fields[]=minimum_free_delivery_amount&fields[]=minimum_order_description&fields[]=minimum_invoice_amount&fields[]=opening_hours&fields[]=is_online_payment&fields[]=status&fields[]=supports&geohash=wssu3x04c6b&is_premium=0&offset=0&type=geohash",
            "https://ss1.bdstatic.com/-0U0bXSm1A5BphGlnYG/tam-ogel/b0548acca9c44289f54585d0d7309b1c_121_121.jpg"),
    BAIDU(3,"百度外卖",
            "http://waimai.baidu.com/waimai/shoplist/442e3423afb7be50?display=json&page=1&count=1000",
            "https://ss0.bdstatic.com/-0U0bnSm1A5BphGlnYG/tam-ogel/c886e507092f2a60843ee9d8181ddd60_121_121.jpg");

    private final int code;
    private final String name;
    private final String url;
    private final String picUrl;

    OrderPlatform(int code, String name, String url, String picUrl){
        this.code = code;
        this.name = name;
        this.url = url;
        this.picUrl = picUrl;
    }

    public String getName(){
        return this.name;
    }

    public String getUrl(){
        return this.url;
    }

    public int getCode(){
        return this.code;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public static OrderPlatform getOpf(int code){
        switch (code){
        case 1:
            return MEITUAN;
        case 2:
            return ELM;
        case 3:
            return BAIDU;
        default:
            return MEITUAN;
        }
    }
}
