package com.hzaupj.pindan.json;

import java.util.List;

/**
 * @Description: MX_N
 * @author: pengjian_91
 * @date: 2015/5/4
 * @version: V1.0
 */
public class JFoodListForElm {

    private List<JFoodForElm> with_image;
    private List<JFoodForElm> without_image;

    public List<JFoodForElm> getWith_image() {
        return with_image;
    }

    public void setWith_image(List<JFoodForElm> with_image) {
        this.with_image = with_image;
    }

    public List<JFoodForElm> getWithout_image() {
        return without_image;
    }

    public void setWithout_image(List<JFoodForElm> without_image) {
        this.without_image = without_image;
    }
}
