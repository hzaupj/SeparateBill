package com.hzaupj.pindan.json;

/**
 * @Description: MX_N
 * @author: pengjian_91
 * @date: 2015/5/4
 * @version: V1.0
 */
public class JFoodCategForElm {
    private String categ;
    private JFoodListForElm foods;

    public String getCateg() {
        return categ;
    }

    public void setCateg(String categ) {
        this.categ = categ;
    }

    public JFoodListForElm getFoods() {
        return foods;
    }

    public void setFoods(JFoodListForElm foods) {
        this.foods = foods;
    }
}
