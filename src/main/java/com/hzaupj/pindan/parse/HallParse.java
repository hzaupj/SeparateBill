package com.hzaupj.pindan.parse;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.hzaupj.pindan.OrderPlatform;
import com.hzaupj.pindan.vo.Hall;
import com.hzaupj.pindan.vo.PlatForm;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by pengjian_91 on 2015/3/10.
 */
public class HallParse {

	public static String MT_PLACECODE = "wssu98hv34jd";

	public static List<Hall> getHallList(OrderPlatform opf) throws IOException {
		List<Hall> resultList = new ArrayList<Hall>();
		switch (opf) {
		case MEITUAN:
			resultList = getHallListForMeituan(opf.getUrl());
			break;
		case ELM:
			resultList = getHallListForElm(opf.getUrl());
			break;
		case BAIDU:
			resultList = getHallListForBaidu(opf.getUrl());
			break;
		default:
			break;
		}
		return resultList;
	}

	private static List<Hall> getHallListForMeituan(String url) throws IOException {
		List<Hall> halls = new ArrayList<Hall>();
		Document doc = Jsoup.connect(url).timeout(5000)
				.userAgent(
						"Mozilla/5.0 (Windows NT 6.1; WOW64)" + UUID.randomUUID().toString())
				.cookie("REMOVE_ACT_ALERT", "1")
				.get();
		Elements eles = doc.select("div").select("[data-title]");
		for (Element element : eles) {
			Hall hall = new Hall();
			hall.setName(element.attr("data-title"));
			hall.setBulletinInfo(element.attr("data-bulletin"));
			String[] otherInfos = getOtherInfo(element);
			hall.setOtherInfo(otherInfos[0] + " " + otherInfos[1]);
			hall.setStartPrice(Integer.valueOf(otherInfos[2]) * 10);
			hall.setSendPrice(Integer.valueOf(otherInfos[3]) * 10);
			String[] infos = getInfo(element);
			hall.setId(infos[0]);
			hall.setPicUrl(infos[1]);
			String disInfo = getDisInfo(element);
			hall.setDisList(getDisListForMeiTuan(disInfo));
			hall.setDisInfo(disInfo);
			hall.setEnable(isEnable(element));
			hall.setPlatform(new PlatForm(OrderPlatform.MEITUAN));
			halls.add(hall);
		}
		return halls;
	}

	private static String[] getOtherInfo(Element element) {
		String[] results = new String[4];
		Elements els = element.select("span").select("[class=start-price]");
		for (Element el : els) {
			results[0] = el.text();
			break;
		}

		els = element.select("span").select("[class=send-price]");
		for (Element el : els) {
			results[1] = el.text();
			break;
		}

		String patternStr = "\\d+.?\\d*";
		Pattern pattern = Pattern.compile(patternStr);
		Matcher m = pattern.matcher(results[0]);
		if (m.find()) {
			results[2] = m.group();
		}

		m = pattern.matcher(results[1]);
		if (m.find()) {
			results[3] = m.group();
		} else {
			results[3] = "0";
		}

		return results;
	}

	private static List<Hall> getHallListForElm(String url) throws IOException {
		List<Hall> halls = new ArrayList<Hall>();
		HttpGet httpGet = new HttpGet(url);
		HttpClient client = HttpClients.createDefault();
		HttpResponse response = client.execute(httpGet);
		Header[] cookie = response.getHeaders("Cookie");
		HttpEntity entity = response.getEntity();
		JSONArray array = JSONObject.parseArray(EntityUtils.toString(entity));
		for (Object object : array) {
			JSONObject jsonObject = (JSONObject) object;
			Hall hall = new Hall();
			hall.setId(jsonObject.getString("name_for_url"));
			hall.setName(jsonObject.getString("name"));
			hall.setPicUrl("http://fuss10.elemecdn.com" + jsonObject.getString("image_path"));
			hall.setBulletinInfo(jsonObject.getString("promotion_info"));
			hall.setStartPrice(jsonObject.getInteger("minimum_order_amount") * 10);

			int delivery = jsonObject.getInteger("delivery_fee");
			String deliveryStr = "免费配送";
			if (delivery != 0) {
				deliveryStr = "配送费" + delivery + "元";
			}
			hall.setOtherInfo(hall.getStartPrice() / 10 + "元起送 " + deliveryStr);
			hall.setSendPrice(delivery * 10);
			hall.setEnable(jsonObject.getBoolean("is_in_book_time") ? 1 : 0);
			hall.setDisList(getDisListForElm(jsonObject));
			hall.setDisInfo(getDisInfoForElm(jsonObject));
			hall.setPlatform(new PlatForm(OrderPlatform.ELM));
			halls.add(hall);
		}
		return halls;
	}

	private static List<Hall> getHallListForBaidu(String url) throws IOException {
		List<Hall> resultList = new ArrayList<Hall>();

		try {
			HttpGet httpGet = new HttpGet(url);
			HttpClient client = HttpClients.createDefault();
			HttpResponse response = null;
			response = client.execute(httpGet);
			HttpEntity entity = response.getEntity();

			String content = EntityUtils.toString(entity);
			JSONArray hallListArray = JSONObject.parseObject(content)
					.getJSONObject("result")
					.getJSONArray("shop_info");

			Hall hall = null;
			JSONObject hallObject = null;
			for (int i = 0, len = hallListArray.size(); i < len; i++) {
				hall = new Hall();
				hallObject = hallListArray.getJSONObject(i);
				hall.setName(hallObject.getString("shop_name"));

				hall.setOtherInfo(
						hallObject.getInteger("takeout_price") + "元起送 配送费 " + hallObject.getInteger("takeout_cost") + "元");
				hall.setId(hallObject.getString("shop_id"));
				String thumbUrl = String
						.format("http://webmap0.map.bdimg.com/maps/services/thumbnails?width=228&height=140&align=center,"
										+ "center&quality=100&src=%s",
								hallObject.getString("logo_url"));
				hall.setPicUrl(thumbUrl);
				hall.setBulletinInfo(hallObject.getString("shop_announcement"));
				hall.setStartPrice(hallObject.getInteger("takeout_price") * 10);
				hall.setDisInfo(getDisInfoForBaidu(hallObject.getJSONObject("discount_info")));
				hall.setSendPrice(hallObject.getInteger("takeout_cost") * 10);
				hall.setEnable(hallObject.getInteger("is_online"));
				hall.setDisList(getDisListForBaidu(hallObject.getJSONObject("discount_info")));
				hall.setPlatform(new PlatForm(OrderPlatform.BAIDU));

				resultList.add(hall);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

		return resultList;
	}

	private static int getDeliveryAmountForElm(Integer minimum_order_amount, Integer minimum_free_delivery_amount) {
		int delivery = 0;
		if (minimum_free_delivery_amount > 0) {
			delivery = minimum_free_delivery_amount - minimum_order_amount;
		}
		return delivery;
	}

	private static String getDisInfoForElm(JSONObject jsonObject) {
		StringBuilder result = new StringBuilder();
		JSONArray array = jsonObject.getJSONArray("restaurant_activity");
		if (array == null || array.isEmpty()) {
			return result.toString();
		}
		for (Object o : array) {
			JSONObject jo = (JSONObject) o;
			result.append(jo.getString("description")).append(";");
		}
		if (result.length() > 0) {
			result.setLength(result.length() - 1);
		}
		return result.toString();
	}

	private static List<String[]> getDisListForElm(JSONObject jsonObject) {
		List<String[]> resultList = new ArrayList<String[]>();
		JSONArray array = jsonObject.getJSONArray("restaurant_activity");
		if (array == null || array.isEmpty()) {
			return resultList;
		}

		for (Object o : array) {
			JSONObject jo = (JSONObject) o;
			int type = jo.getInteger("type");
			if (type == 102) {
				String attr = jo.getString("attribute");
				Map<String, Map<String, Integer>> disMap = JSONObject.parseObject(attr, Map.class);
				Iterator<String> it = disMap.keySet().iterator();
				while (it.hasNext()) {
					String fullPic = it.next();
					int disPic = disMap.get(fullPic).get("1");
					resultList.add(new String[] { fullPic, String.valueOf(disPic) });
				}
			}
		}
		return resultList;
	}

	private static List<String[]> getDisListForMeiTuan(String disInfo) {
		List<String[]> results = new ArrayList<String[]>();
		String patternStr = "满(\\S*?)元减(\\S*?)元";
		Pattern pattern = Pattern.compile(patternStr);
		Matcher m = pattern.matcher(disInfo);
		while (m.find()) {
			String[] dis = new String[2];
			dis[0] = m.group(1);
			dis[1] = m.group(2);
			results.add(dis);
		}

		return results;
	}

	/**
	 * 获取活动信息
	 *
	 * @param disInfo
	 * @return
	 */
	private static String getDisInfoForBaidu(JSONObject disInfo) {
		return disInfo.getString("discount_first_order_show");
	}

	/**
	 * 获取活动信息
	 *
	 * @param disInfo
	 * @return
	 */
	private static List<String[]> getDisListForBaidu(JSONObject disInfo) {
		List<String[]> results = new ArrayList<String[]>();

		Pattern pattern = Pattern.compile("满(\\d*)元?减(\\d*)元?");
		Matcher m = pattern.matcher(disInfo.getString("discount_first_order_show"));
		while (m.find()) {
			String[] dis = new String[2];
			dis[0] = m.group(1);
			dis[1] = m.group(2);

			results.add(dis);
		}

		return results;
	}

	private static String[] getInfo(Element element) {
		String[] results = new String[2];
		Elements els = element.select("img").select("[data-rid]");
		for (Element el : els) {
			String id = el.attr("data-rid");
			results[0] = id;
			String url = el.attr("data-src");
			results[1] = url;
		}
		return results;
	}

	private static int isEnable(Element element) {
		Elements els = element.select("div").select("[class*=outof-sale]");
		if (els.isEmpty()) {
			return 1;
		} else {
			return 0;
		}
	}

	private static String getDisInfo(Element element) {
		Elements els = element.select("div").select("[class=others]");
		for (Element el : els) {
			return el.data();
		}
		return null;
	}
}
