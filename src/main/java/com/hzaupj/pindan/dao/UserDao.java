package com.hzaupj.pindan.dao;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;

import java.util.Date;
import java.util.UUID;

/**
 * @Description: MX_N
 * @author: pengjian_91
 * @date: 2015/4/7
 * @version: V1.0
 */
public class UserDao {

    public static Record inquireBySid(String sid) {
        Record session = Db.findFirst("select * from session where sid = ?", sid);
        if(session == null){
            return null;
        }
        return Db.findFirst("select * from user where id = ?", session.getLong("user_id"));
    }

    public static Record inquireByAccount(String account) {
        return Db.findFirst("select * from user where account = ?", account);
    }

    public static Record inquireById(long userId) {
        return Db.findById("user", userId);
    }

    public static void saveSid(long userId, String sid) {
        Record session = Db.findFirst("select * from session where user_id = ?", userId);
        if(session == null){
            session = new Record().set("sid", sid).set("user_id", userId);
            Db.save("session", session);
        }else{
            session.set("sid", sid);
            Db.update("session", session);
        }
    }

    public static String addUser(String account, String name, String password, int floor) {
        Record user = new Record()
                .set("name", name).set("account", account)
                .set("password", password).set("register_time", new Date())
                .set("floor", floor).set("last_login_time", new Date());
        Db.save("user", user);

        String sid = UUID.randomUUID().toString();
        saveSid(user.getLong("id"), sid);
        return sid;
    }

    public static long count() {
        return Db.queryLong("select count(id) from user");
    }
}
