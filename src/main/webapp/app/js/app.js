'use strict';
/* App Module */

var spbApp = angular.module('spbApp', [
  "ngStorage",
  "ngSanitize",
  'ui.router',
  'ui.bootstrap',
  'w5c.validator',
  'angular-md5',
  'oitozero.ngSweetAlert',
  'spbServices',
  'userModule',
  'shopModule',
  'orderModule',
  'reqorderModule',
  'angular-sortable-view',
  // 'ui.tree',
  // 'ng-sortable',
  // 'spbFilters',
]);
// 
/**
 * 由于整个应用都会和路由打交道，所以这里把$state和$stateParams这两个对象放到$rootScope上，方便其它地方引用和注入。
 * 这里的run方法只会在angular启动的时候运行一次。
 * @param  {[type]} $rootScope
 * @param  {[type]} $state
 * @param  {[type]} $stateParams
 * @return {[type]}
 */
spbApp.run(function($rootScope, $state, $stateParams, $localStorage) {
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;
    $rootScope.gobal = {};
    // $rootScope.isLoggedIn = SecurityApi;
    });

/**
 * 配置路由。
 * 注意这里采用的是ui-router这个路由，而不是ng原生的路由。
 * ng原生的路由不能支持嵌套视图，所以这里必须使用ui-router。
 * @param  {[type]} $stateProvider
 * @param  {[type]} $urlRouterProvider
 * @return {[type]}
 */


spbApp.config(function($stateProvider, $locationProvider, $urlRouterProvider, w5cValidatorProvider) {
      console.log("配置注入成功");
      w5cValidatorProvider.config({
          blurTrig: false,
          showError: true,
          removeError: true
      });

      w5cValidatorProvider.setRules({
          userAccount: {
              required: "帐号不能为空",
              pattern: "帐号必须输入字母、数字、下划线",
          },
          userName: {
              required: "输入的用户名不能为空",
          },
          userPwd: {
              required: "密码不能为空",
              minlength: "密码长度不能小于{minlength}",
              maxlength: "密码长度不能大于{maxlength}"
          },
          userRepeatPwd: {
              required: "重复密码不能为空",
              repeat: "两次密码输入不一致"
          },
          userOriginalPwd: {
              required: "输入的原始密码不能为空"
          },
          userNewPwd     : {
              required: "输入的新密码不能为空"
          },
          userFloor: {
              required: "请输入数字楼层",
          },
          commPercent: {
              required: "请输入3-20之间的整数，请不要包含小数点",
          },
      });

    // $locationProvider.html5Mode(true);
    $urlRouterProvider.otherwise('/index');
    $stateProvider
        .state('index',{
            url:'/index',
            templateUrl: 'html/home.html',
            onEnter: ["$state", "$stateParams", "$timeout", "Api", function ($state, $stateParams, $timeout, Api) {
              var params = {
                  platformId: Api.getPlatformId(),
              }
              if (!Api.getAccount()) {
                  $timeout(function() {
                    $state.go('login');
                  });
              }else if (!Api.getPlatformId()) {
                $timeout(function() {
                    $state.go('platform');
                })
              }else{
                $timeout(function() {
                  $state.go('hallList',params,{reload:true});
                })
              }
            }]
        })
        .state('login', {
            url: '/login',
            templateUrl: 'html/login.html',
            controller: "userLoginCtrl",
            resolve: {  }
        })
        .state('register', {
            url: '/register', 
            templateUrl: 'html/register.html',
            controller: "userRegisterCtrl"
        })
        .state('platform',{
            url:'/platform',
            templateUrl: 'html/platform.html',
            controller: 'platformCtrl'
        })
        .state('hallList',{
            url:'/hallList/{platformId:[0-9]}',
            templateUrl: 'html/home.html',
            controller: 'shopListCtrl'
        })
        .state('foodList',{
            url:'/foodList/{platformId:[0-9]}?hallId',
            templateUrl: 'html/shop.html',
            controller: 'shopDetailCtrl'
        })
        .state('order', {
            url: '/order',
            // url: '/pindanList/{stateId:[0-9]}',
            templateUrl: 'html/order.html',
            controller: 'orderListCtrl',
        })
        .state('reqorder', {
            url: '/reqorder',
            templateUrl: 'html/request-order.html',
            controller: 'reqorderListCtrl',
        })
        .state('orderDetail', {
            // url: '/orderDetail',
            url: '/orderDetail/{platformId:[0-9]}?hallId?pindanId?foodId',
            templateUrl: 'html/order-detail.html',
            controller: 'orderDetailCtrl',
        })
        .state('book', {
            url: '/book/{platformId:[0-9]}?hallId?pindanId?hasPublic',
            templateUrl: 'html/order-shop.html',
            controller: 'bookCtrl',
        })
        .state('profile', {
            url: '/profile',
            views :{
              '': {
                  templateUrl: 'html/profile.html'
              },
              'main@profile': {
                  templateUrl: 'html/profile-info.html'
              }
            }
        })
        .state('profile.info', {
            url: '/info',
            views: {
              'main@profile':{
                templateUrl: 'html/profile-info.html',
                controller: 'userInfoCtrl'
              }
            }
        })
        .state('profile.security', {
            url: '/security',
            views: {
              'main@profile':{
                templateUrl: 'html/profile-security.html',
                controller: 'userSecurityCtrl'
              }
            }
        })
});

