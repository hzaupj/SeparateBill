/* 用户登录&注册 */

'user strict';
var userModule = angular.module('userModule', ['userControllers', 'spbServices', 'w5c.validator']);

/* Controllers */
var userControllers = angular.module('userControllers', ['ngResource']);

userControllers.controller('userLoginCtrl', ['$scope', '$state', '$localStorage', 'md5', 'Api',
    function($scope, $state, $localStorage, md5, Api) {
        var vm = $scope.vm = {
            showErrorType: 1
        };
        vm.validateOptions = {
            blurTrig: true
        };
        //删除单条警告
        $scope.closeAlert = function (index) {
            $scope.alerts.splice(index, 1);
        };
        $scope.alerts = [];

        $scope.has_msg = false;
        $scope.login = function(form) {
            
            if(form.$valid){
                Api.user.login.query({
                    account:$scope.account,
                    password:md5.createHash($scope.password)
                }).$promise.then(
                    function(data,status) {
                        if (data.msg) {
                            $scope.has_msg = true;
                            $scope.alerts = [
                                {type:'danger',msg:data.msg},
                            ];
                        } else {
                            $localStorage.loggedIn = true;
                            $localStorage.account = $scope.account;
                            $state.go('index');
                        }
                    },
                    function() {
                        console.log("请求出错");
                    }
                );
            }
        };
    }
]);

//用户状态
userControllers.controller('userStatusCtrl', ['$scope', '$state', '$localStorage', 'Api',
    function($scope, $state, $localStorage, Api){
        $scope.logout = function(){
            Api.user.logout.query();
            $localStorage.account = null;
            $localStorage.loggedIn = false;
            $localStorage.platformId = null;
            $state.go('login');
        }
    }
]);

/* 注册 */
userControllers.controller('userRegisterCtrl', ['$scope', '$state', '$localStorage', 'md5', 'Api', 'SweetAlert',
    function($scope, $state, $localStorage, md5, Api, SweetAlert) {
        var vm = $scope.vm = {
            showErrorType: 1
        };
        vm.validateOptions = {
            blurTrig: true
        };
        $scope.register_ing = false;
        $scope.has_msg = false;
        
        //删除单条警告
        $scope.closeAlert = function (index) {
            $scope.alerts.splice(index, 1);
        };
        $scope.alerts = [];
        $scope.register = function(form) {
            if(form.$valid){
                $scope.register_ing = true;
                Api.user.register.query({
                    account:$scope.account,
                    name:$scope.username,
                    floor:$scope.floor,
                    password:md5.createHash($scope.password),
                    again_password:md5.createHash($scope.repeatPwd)
                }).$promise.then(
                    function(data,status) {
                        if (data.flag === "SUCCESS") {
                            $localStorage.loggedIn = true;
                            $localStorage.account = $scope.account;
                            SweetAlert.swal({
                                title:"注册成功", 
                                text:"将跳转到个人信息页面", 
                                type:"success"
                            },function(){
                                $state.go("profile.info");
                            });
                        }else if (data.msg) {
                            $scope.has_msg = true;
                            $scope.alerts = [
                                {type:'danger',msg:data.msg},
                            ];
                        } 
                    },
                    function() {
                        console.log("请求出错");
                    }
                );
            }
        };
    }
]);

/* 用户信息 */
userControllers.controller('userInfoCtrl', ['$scope', '$state', 'Api',
    function($scope, $state, Api) {
        var vm = $scope.vm = {
            showErrorType: 1
        };
        vm.validateOptions = {
            blurTrig: true
        };
        //删除单条警告
        $scope.closeAlert = function (index) {
            $scope.alerts.splice(index, 1);
        };
        $scope.has_msg = false;
        $scope.user = {};
        $scope.alerts = [];
        //删除单条警告
        $scope.closeAlert = function (index) {
            $scope.alerts.splice(index, 1);
        };
        Api.user.userbasic.query({})
        .$promise.then(
            function(data,status) {
                if(data.flag === "SUCCESS"){
                    $scope.user = data.result;
                }else if(data.flag == "UNLOGIN"){
                     $state.go('login')
                }
            },
            function() {
                console.log("请求出错");
            }
        );
        $scope.changeBasic = function (form) {
            if(form.$valid){
                Api.user.update.query({
                    account:$scope.user.account,
                    name:$scope.user.name,
                    floor:$scope.user.floor,
                    weixinNo:$scope.user.weixin_no,
                    alipayNo:$scope.user.alipay_no,
                    alipayCodeUrl:$scope.user.alipay_code_url,
                }).$promise.then(
                    function(data,status) {
                        var flag = data.flag;
                        if ( flag === "SUCCESS" ) {
                            $scope.has_msg = true;
                            $scope.alerts = [
                                {type:'success',msg:data.msg},
                            ];
                        }else{
                            $scope.has_msg = true;
                            $scope.alerts = [
                                {type:'danger',msg:data.msg},
                            ];
                        } 
                    },
                    function() {
                        console.log("请求出错");
                    }
                );
            }
        }
    }
]);



/* 修改密码 */
userControllers.controller('userSecurityCtrl', ['$scope', '$state', 'md5', 'Api',
    function($scope, $state, md5, Api) {
        var vm = $scope.vm = {
            showErrorType: 1
        };
        vm.validateOptions = {
            blurTrig: true
        };
        //删除单条警告
        $scope.closeAlert = function (index) {
            $scope.alerts.splice(index, 1);
        };
        $scope.has_msg = false;
        $scope.user = {};
        $scope.alerts = [];

        Api.user.userbasic.query({})
        .$promise.then(
            function(data,status) {
                if(data.result){
                    $scope.user = data.result;
                }else if(data.flag == "UNLOGIN"){
                     $state.go('login')
                }
            },
            function() {
                console.log("请求出错");
            }
        );
        $scope.changePwd = function (form) {
            if(form.$valid){
                Api.user.updatePwd.query({
                    account:$scope.user.account,
                    old_password:md5.createHash($scope.user.password),
                    new_password:md5.createHash($scope.user.newPwd),
                    new2_password:md5.createHash($scope.user.repeatPwd),
                }).$promise.then(
                    function(data,status) {
                        var flag = data.flag;
                        if ( flag === "SUCCESS" ) {
                            $scope.has_msg = true;
                            $scope.alerts = [
                                {type:'success',msg:data.msg},
                            ];
                        }else{
                            $scope.has_msg = true;
                            $scope.alerts = [
                                {type:'danger',msg:data.msg},
                            ];
                        } 
                    },
                    function() {
                        console.log("请求出错");
                    }
                );
            }
        }
    }
]);





