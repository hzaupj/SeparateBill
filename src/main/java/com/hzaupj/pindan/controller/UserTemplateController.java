package com.hzaupj.pindan.controller;

import com.hzaupj.pindan.MainConfig;
import com.hzaupj.pindan.dao.UserDao;
import com.hzaupj.pindan.service.UserService;
import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.activerecord.tx.Tx;
import jodd.util.StringUtil;

/**
 * MainController
 */
public class UserTemplateController extends Controller {

	/**
	 * 登录
	 * @throws Exception
	 */
	@Before(Tx.class)
	public void login() throws Exception {
		String account = getPara("account");
		String password = getPara("password");

		Record user = UserDao.inquireByAccount(account);
		if(user == null) {
			renderText("用户名错误！");
			return;
		}

		if(!user.getStr("password").equals(password)){
			renderText("密码错误！");
			return;
		}

		String sid = UserService.login(user);

		setCookie("sid", sid, MainConfig.MAX_COOKIE_TIME);
		renderText("ok");
	}

	/**
	 * 退出
	 * @throws Exception
	 */
	@Before(Tx.class)
	public void logout() throws Exception {
		removeCookie("sid");
		renderText("ok");
	}


	/**
	 * 注册
	 * @throws Exception
	 */
	@Before(Tx.class)
	public void register() throws Exception {
		String account = getPara("account");
		String name = getPara("name");
        Integer floor = getParaToInt("floor");
		String password = getPara("password");
		String again_password = getPara("again_password");

        if(StringUtil.isEmpty(account)){
            renderText("工号不能为空！");
            return;
        }
        if(StringUtil.isEmpty(name)){
            renderText("姓名不能为空！");
            return;
        }

        if(floor == null || floor <=0){
            renderText("楼层不能为空！");
            return;
        }

        if(StringUtil.isEmpty(password)){
            renderText("密码不能为空！");
            return;
        }

		if(!password.equals(again_password)){
			renderText("两次密码输入不一致！");
			return;
		}

		Record user = UserDao.inquireByAccount(account);
		if(user!=null){
			renderText("该工号已被注册！");
			return;
		}

		String sid = UserService.register(account, name, password, floor);

		setCookie("sid", sid, MainConfig.MAX_COOKIE_TIME);
		renderText("ok");
	}

	/**
	 * 用户信息
	 * @throws Exception
	 */
	public void userInfo() throws Exception {
		Record user = UserService.getUserInfo();
		setAttr("account", user.getStr("account"));
		setAttr("name", user.getStr("name"));
        setAttr("floor", user.getInt("floor"));
		setAttr("alipay_no", user.getStr("alipay_no") == null ? "" :user.getStr("alipay_no"));
		setAttr("alipay_code_url", user.getStr("alipay_code_url") == null ? "" :user.getStr("alipay_code_url"));
		setAttr("weixin_no", user.getStr("weixin_no") == null ? "" :user.getStr("weixin_no"));

		render("/template/userinfo.html");
	}

	/**
	 * 保存用户信息
	 * @throws Exception
	 */
	@Before(Tx.class)
	public void save() throws Exception {
		String account = getPara("account");
		String name = getPara("name");
        Integer floor = getParaToInt("floor");
		String weixinNo = getPara("weixinNo");
		String alipayNo = getPara("alipayNo");
		String alipayCodeUrl = getPara("alipayCodeUrl");

        if(StringUtil.isEmpty(account)){
            renderText("工号不能为空！");
            return;
        }
        if(StringUtil.isEmpty(name)){
            renderText("姓名不能为空！");
            return;
        }

        if(floor == null || floor <=0){
            renderText("楼层数必须大于0！");
            return;
        }

		Record user = UserDao.inquireByAccount(account);
		if(user == null){
			renderText("账号不存在！");
			return;
		}

		UserService.save(user, name, alipayNo, weixinNo, alipayCodeUrl, floor);

		renderText("ok");
	}

	/**
	 * 修改密码
	 * @throws Exception
	 */
	@Before(Tx.class)
	public void updatePassword() throws Exception {
		String account = getPara("account");
		String old_password = getPara("old_password");
		String new_password = getPara("new_password");
		String new2_password = getPara("new2_password");

		if(!new_password.equals(new2_password)){
			renderText("两次新密码输入不一致！");
			return;
		}

		Record user = UserDao.inquireByAccount(account);
		if(user == null){
			renderText("账号不存在！");
			return;
		}

		if(!old_password.equals(user.getStr("password"))){
			renderText("旧密码输入错误！");
			return;
		}

		UserService.updatePassword(user, new_password);

		renderText("密码修改成功！");
	}
}





