package com.hzaupj.pindan.controller;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.jfinal.core.Controller;

import java.util.HashMap;
import java.util.Map;

/**
 * @Description: MX_N
 * @author: pengjian_91
 * @date: 2015/5/8
 * @version: V1.0
 */
public class BaseJsonController extends Controller {

    private final String SUCCESS = "SUCCESS";
    private final String FAIL = "FAIL";
    private final String UNLOGIN = "UNLOGIN";
    protected final String PASSWORD_ERROR = "PASSWORD_ERROR";
    protected final String ACCOUNT_ERROR = "ACCOUNT_ERROR";
    /**
     * 统一输出
     *
     */
    public void renderUnLoginJson(){
        renderJson(UNLOGIN, null, null);
    }

    /**
     * 统一输出
     *
     */
    public void renderOkJson(){
        renderJson(SUCCESS, null, null);
    }

    /**
     * 统一输出
     *
     */
    public void renderOkJson(String msg){
        renderJson(SUCCESS, msg, null);
    }

    /**
     * 统一输出
     *
     */
    public void renderOkJson(Map<String, Object> resultMap){
        renderJson(SUCCESS, null, resultMap);
    }

    /**
     * 统一输出
     *
     */
    public void renderErrorJson(String msg){
        renderJson(FAIL, msg, null);
    }


    /**
     * 统一输出
     *
     * @param flag
     * @param msg
     * @param resultMap
     */
    public void renderJson(String flag, String msg, Map<String, Object> resultMap){
        StringBuilder result = new StringBuilder();

        Map<String, Object> resultJsonMap = new HashMap<String, Object>();
        resultJsonMap.put("flag", flag);
        resultJsonMap.put("msg", msg);
        resultJsonMap.put("result", resultMap);

        result.append(getAttrForStr("callback")).append("(")
                .append(JSONObject.toJSONString(resultJsonMap, SerializerFeature.DisableCircularReferenceDetect))
                .append(");");
        renderText(result.toString());
    }
}
