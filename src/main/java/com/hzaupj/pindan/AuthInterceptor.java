package com.hzaupj.pindan;

import com.hzaupj.pindan.dao.UserDao;
import com.hzaupj.pindan.service.MainService;
import com.jfinal.aop.Interceptor;
import com.jfinal.core.ActionInvocation;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Record;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description: MX_N
 * @author: pengjian_91
 * @date: 2015/4/7
 * @version: V1.0
 */
public class AuthInterceptor implements Interceptor {

    private static List<String> ignoreActionKey = new ArrayList<String>();
    static {
        ignoreActionKey.add("/main/login");
        ignoreActionKey.add("/main/register");
        ignoreActionKey.add("/user/login");
        ignoreActionKey.add("/user/register");
    }
    @Override
    public void intercept(ActionInvocation actionInvocation) {
        String actionKey = actionInvocation.getActionKey();
        if(ignoreActionKey.contains(actionKey)){
            actionInvocation.invoke();
            return;
        }

        Controller controller = actionInvocation.getController();
        String sid = controller.getCookie("sid");
        if(sid == null || sid.isEmpty()){
            controller.render("/template/login.html");
            return;
        }

        Record user = UserDao.inquireBySid(sid);
        if(user == null){
            controller.render("/template/login.html");
            return;
        }

        controller.setAttr("user", user);
        controller.setAttr("accountCount", UserDao.count());
        controller.setAttr("requestShareNum", MainService.getReqShareOrderMap().size());

        if(!actionKey.equals("/user/save") && (user.getInt("floor") == null || user.getInt("floor") <=0)){
            controller.setAttr("account", user.getStr("account"));
            controller.setAttr("name", user.getStr("name"));
            controller.setAttr("floor", user.getInt("floor") == null ? 0 : user.getInt("floor"));
            controller.setAttr("alipay_no", user.getStr("alipay_no") == null ? "" : user.getStr("alipay_no"));
            controller.setAttr("alipay_code_url",
                    user.getStr("alipay_code_url") == null ? "" : user.getStr("alipay_code_url"));
            controller.setAttr("weixin_no", user.getStr("weixin_no") == null ? "" : user.getStr("weixin_no"));

            controller.render("/template/userinfo.html");
            return;
        }

        MainConfig.saveLoginUser(user);
        actionInvocation.invoke();
    }
}
