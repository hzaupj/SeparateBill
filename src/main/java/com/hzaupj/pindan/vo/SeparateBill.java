package com.hzaupj.pindan.vo;

import java.util.List;

/**
 * @Description: MX_N
 * @author: pengjian_91
 * @date: 2015/7/10
 * @version: V1.0
 */
public class SeparateBill {
	private String id;
	private String[] discounts;
	private List<Order> orderList;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<Order> getOrderList() {
		return orderList;
	}

	public void setOrderList(List<Order> orderList) {
		this.orderList = orderList;
	}

	public String[] getDiscounts() {
		return discounts;
	}

	public void setDiscounts(String[] discounts) {
		this.discounts = discounts;
	}
}
