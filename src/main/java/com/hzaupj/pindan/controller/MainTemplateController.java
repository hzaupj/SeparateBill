package com.hzaupj.pindan.controller;

import com.alibaba.fastjson.JSONObject;
import com.hzaupj.pindan.MainConfig;
import com.hzaupj.pindan.OrderPlatform;
import com.hzaupj.pindan.dao.UserDao;
import com.hzaupj.pindan.json.JFood;
import com.hzaupj.pindan.json.JShopCart;
import com.hzaupj.pindan.service.MainService;
import com.hzaupj.pindan.vo.*;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Record;
import jodd.util.StringUtil;
import org.jsoup.Connection;
import org.jsoup.Jsoup;

import java.io.IOException;
import java.net.URLDecoder;
import java.util.*;

/**
 * MainController
 */
public class MainTemplateController extends Controller {

    /**
     * 登录页面
     *
     * @throws Exception
     */
    public void login() throws Exception {
        render("/template/login.html");
    }

    /**
     * 注册页面
     *
     * @throws Exception
     */
    public void register() throws Exception {
        render("/template/register.html");
    }

    /**
     * 首页 - 订餐平台选择
     *
     * @throws Exception
     */
    public void index() throws Exception {
        render("/template/index.html");
    }

    /**
     * 餐厅列表
     * @throws Exception
     */
    public void hallList() throws Exception {
        int opf = getParaToInt(0);

        MainService.hallList(opf);

        setAttr("platform", OrderPlatform.getOpf(opf));
        setAttr("row", Math.ceil(MainService.getHallTable().row(opf).size() / 4.0));
        setAttr("hallList", new ArrayList(MainService.getHallTable().row(opf).values()));
        render("/template/halllist.html");
    }

    /**
     * 食物列表
     *
     * @throws Exception
     */
    public void foodList() throws Exception {
        int platformId = getParaToInt(0);
        String hallId = getPara("hallId");
        String pindanId = getPara("pindanId");
        MainService.foodList(platformId, hallId);

        Hall hall = MainService.getHallTable().row(platformId).get(hallId);
        double row = Math.ceil(MainService.getFoodTable().row(hallId).size() / 5.0);
        Collection<Food> list = MainService.getFoodTable().row(hallId).values();

        setAttr("platform", OrderPlatform.getOpf(platformId));
        setAttr("foodList", new ArrayList(list));
        setAttr("pindanId", pindanId);
        setAttr("row", row);
        setAttr("hall", hall);

        render("/template/foodlist.html");
    }

    /**
     * 拼单列表
     *
     * @throws Exception
     */
    public void pindanList() throws Exception {
        Integer status = getParaToInt(0);
        if(status == null){
            status = -1;
        }

        long loginUserId = MainConfig.getLoginUser().getLong("id");
        // 我发起
        Map<String, PinDan> myStartPindanMap = new HashMap<String, PinDan>();
        // 我参与
        Map<String, PinDan> myPartPindanMap = new HashMap<String, PinDan>();

        Map<String, Record> userMap = new HashMap<String, Record>();
        Map<String, Integer[]> orderInfoMap = new HashMap<String, Integer[]>();
        Iterator<String> it = MainService.getPindanMap().keySet().iterator();
        while (it.hasNext()){
            String pindanId = it.next();
            PinDan pindan = MainService.getPindanMap().get(pindanId);
            int orderNum = MainService.getOrderNum(pindanId);
            int orderMoney = MainService.getOrderMoney(pindanId);
            orderInfoMap.put(pindanId, new Integer[]{orderNum, orderMoney});

            Record user = UserDao.inquireById(pindan.getUserId());
            if(user != null){
                userMap.put(pindanId, user);
            }else{
                userMap.put(pindanId, new Record());
            }

            // 获取我发起的拼单
            if(pindan.getUserId() == loginUserId){
                myStartPindanMap.put(pindanId, pindan);
            }
            // 获取我参与的拼单（包含我发起）
            if(MainService.isPartOfPindan(pindanId, loginUserId)){
                myPartPindanMap.put(pindanId, pindan);
            }
        }

        // 去除包含我发起的
        for(String key : myStartPindanMap.keySet()){
            myPartPindanMap.remove(key);
        }

        Map<String, PinDan> orderPindanMap = MainService.filterPindanMap(status);
        orderPindanMap = MainService.orderPindanMap(orderPindanMap);

        setAttr("orderInfoMap", orderInfoMap);
        setAttr("userMap", userMap);
        setAttr("pindanMap", orderPindanMap);
        setAttr("myStartPindanMap", myStartPindanMap);
        setAttr("myPartPindanMap", myPartPindanMap);

        render("/template/pindanlist.html");
    }

    /**
     * 求拼池列表
     *
     * @throws Exception
     */
    public void requestShareList() throws Exception {
        List<Order> orderList = MainService.requestShareOrderList();
        Map<String, Record> userMap = new HashMap<String, Record>();
        Map<String, Hall> hallMap = new HashMap<String, Hall>();
        if(orderList != null) {
            for (Order order : orderList) {
                // 设置用户Map
                Record user = UserDao.inquireById(order.getUserId());
                if (user != null) {
                    userMap.put(order.getId(), user);
                } else {
                    userMap.put(order.getId(), new Record());
                }

                // 设置餐厅Map
                hallMap.put(order.getId(),
                        MainService.getHallTable()
                                .get(order.getFood().getPlatform().getCode(), order.getFood().getHallId()));
            }
        }
        // 求拼列表按照佣金金额排序
        orderList = MainService.sortReqShareOrderList(orderList);
        // 获取我的拼单列表
        setAttr("myPindanList", MainService.getMyPindanList(MainConfig.getLoginUser().getLong("id")));
        setAttr("orderList", orderList);
        setAttr("userMap", userMap);
        setAttr("hallMap", hallMap);

		render("/template/requestShareList.html");

    }

    /**
     * 新增一个拼单
     *
     * @throws Exception
     */
    public void addPindan() throws Exception {
        int platformId = getParaToInt(0);
        String hallId = getPara("hallId");
        String code = getPara("code");

        if(MainService.getHallTable().row(platformId).isEmpty()){
            renderText("餐厅列表刚才正被刷新，请重试！");
            return;
        }

        boolean isTrueCode = false;
        if(!StringUtil.isEmpty(code)){
            if(code.length() != 4 || !MainService.isNumeric(code)){
                renderText("请输入正确的4位数字作为拼单口令！");
                return;
            }
            isTrueCode = true;
        }
        MainService.addPindan(platformId, hallId, code, isTrueCode);
        renderText("ok");
    }

    /**
     * 添加一个求拼
     *
     * @throws Exception
     */
    public void addRequestShareOrder() throws Exception {
        String hallId = getPara("hallId");
        String foodId = getPara("foodId");
        int maxCommissionRate = getParaToInt("maxCommissionRate");
        String remark = getPara("remark");

        Food selectedFood = MainService.getFoodTable().get(hallId, foodId);
        if (selectedFood == null) {
            renderText("亲，找不到这个餐！");
            return;
        }

        MainService.addRequestShareOrder(hallId, foodId, maxCommissionRate, remark);

        renderText("ok");
    }

    /**
     * 加入拼单
     */
    public void joinPindan() throws Exception{
        String orderId = getPara("orderId");
        String pindanId = getPara("pindanId");

        Order reqShareOrder = MainService.getReqShareOrderMap().get(orderId);
        PinDan pinDan = MainService.getPindanMap().get(pindanId);
        MainService.joinPindan(pinDan, reqShareOrder);

        renderText("ok");
    }

    /**
     * 点餐
     *
     * @throws Exception
     */
    public void order() throws Exception {
        String pindanId = getPara("pindanId");
        String hallId = getPara("hallId");
        String foodId = getPara("foodId");

        Food selectedFood = MainService.getFoodTable().get(hallId, foodId);
        if (selectedFood == null) {
            renderText("亲，找不到这个餐！");
            return;
        }

        PinDan pindan = MainService.getPindanMap().get(pindanId);
        if(pindan == null){
            renderText("拼单不存在");
            return;
        }
        if(pindan.getStatus() == 0){
            renderText("已停止点餐！");
            return;
        }

        MainService.order(pindan, hallId, foodId);

        renderText("ok");
    }

    /**
     * 拼单详情
     *
     * @throws Exception
     */
    public void report() throws Exception {
        String pindanId = getPara("pindanId");
        int platformId = getParaToInt(0);
        String hallId = getPara("hallId");
        if (MainService.getOrderMap() == null) {
            renderText("没有任何人点餐！");
            return;
        }

        PinDan pindan = MainService.getPindanMap().get(pindanId);
        if(pindan == null){
            renderText("拼单已不存在！");
            return;
        }

        Hall hall = MainService.getHallTable().row(platformId).get(hallId);
        List<Order> orderList = MainService.getOrderList(pindan.getId(), MainService.getOrderMap());

        Map<String, SeparateBill> reportMap = MainService.report(pindan, hall, orderList);

        Record owner = UserDao.inquireById(pindan.getUserId());
        Map<Long, Record> userMap = MainService.getUserMap(orderList);

        setAttr("userMap", userMap);
        setAttr("map", reportMap);
        setAttr("hall", hall);
        setAttr("owner", owner);
        setAttr("pindan", pindan);
        render("/template/report.html");
    }

    /**
     * 修改备注
     * @throws Exception
     */
    public void updateRemark() throws Exception {
        String orderId = getPara("orderId");
        String remark = getPara("remark");
        Order order = MainService.getOrderMap().get(orderId);
        if(order == null){
            renderText("订单不存在");
            return;
        }
        order.setRemark(remark);

        renderText("ok");
    }


    /**
     * 修改求拼备注
     * @throws Exception
     */
    public void updateRequestShareRemark() throws Exception {
        String orderId = getPara("orderId");
        String remark = getPara("remark");
        Order order = MainService.getReqShareOrderMap().get(orderId);
        if(order == null){
            renderText("订单不存在");
            return;
        }
        order.setRequestShareRemark(remark);

        renderText("ok");
    }

	/**
	 * 修改转账状态
	 * @throws Exception
	 */
	public void updateIsPay() throws Exception {
		String orderId = getPara("orderId");
		Integer isPay = getParaToInt("isPay");
		Order order = MainService.getOrderMap().get(orderId);
		if(order == null){
			renderText("订单不存在");
			return;
		}
		order.setIsPay(isPay);

		renderText("ok");
	}


	/**
     * 删除订餐
     * @throws Exception
     */
    public void delete() throws Exception {
        String orderId = getPara("orderId");
        Order order = MainService.getOrderMap().get(orderId);
        if(order == null){
            renderText("订单不存在");
            return;
        }
        PinDan pindan = MainService.getPindanMap().get(order.getPindanId());
        if(pindan == null){
            renderText("拼单不存在");
            return;
        }
        if(pindan.getStatus() == 0){
            renderText("已停止点餐，不允许删除！");
            return;
        }

        MainService.delete(order);

        renderText("删除成功");
    }

    /**
     * 删除求拼
     * @throws Exception
     */
    public void deleteReq() throws Exception {
        String orderId = getPara("orderId");
        Order order = MainService.getReqShareOrderMap().get(orderId);
        if(order == null){
            renderText("订单不存在");
            return;
        }

        MainService.deleteReq(order);

        renderText("删除成功");
    }

    /**
     * 删除拼单
     * @throws Exception
     */
    public void deletePindan() throws Exception {
        String pindanId = getPara("pindanId");
        PinDan pindan = MainService.getPindanMap().get(pindanId);
        if(pindan == null){
            renderText("拼单已不存在！");
            return;
        }

        if(pindan.getStatus() == 1){
            renderText("请先停止点餐再删除该拼单！");
            return;
        }

        // 删除拼单
        MainService.deletePindan(pindanId);

        renderText("删除成功");
    }

	/**
	 * 删除拆单
	 * @throws Exception
	 */
	public void deleteSB() throws Exception {
		String pindanId = getPara("pindanId");
		String sbId = getPara("sbId");
		SeparateBill sb = MainService.getSB(pindanId, sbId);
		if(sb != null){
			if(sb.getOrderList() != null && sb.getOrderList().size()>0){
				renderText("只能删除空单！该单下还有订单，请移除后再删除!");
				return;
			}
		}
		MainService.deleteSB(pindanId, sbId);
		renderText("删除成功");
	}

	/**
	 * 新增拆单
	 * @throws Exception
	 */
	public void addSB() throws Exception {
		String pindanId = getPara("pindanId");
		MainService.addSB(pindanId);
		renderText("新增成功");
	}

    /**
     * 启用拼单
     * @throws Exception
     */
    public void start() throws Exception {
        String pindanId = getPara("pindanId");
        PinDan pinDan = MainService.getPindanMap().get(pindanId);
        if(pinDan == null){
            renderText("找不到这个拼单！");
            return;
        }

        MainService.start(pinDan);

        renderText("启用成功");
    }

    /**
     * 停止拼单
     * @throws Exception
     */
    public void stop() throws Exception {
        String pindanId = getPara("pindanId");
        PinDan pinDan = MainService.getPindanMap().get(pindanId);
        if(pinDan == null){
            renderText("找不到这个拼单！");
            return;
        }

        MainService.stop(pinDan);
        renderText("停止成功");
    }

	/**
	 * 打开自动拆单
	 * @throws Exception
	 */
	public void openAuto() throws Exception {
		String pindanId = getPara("pindanId");
		PinDan pinDan = MainService.getPindanMap().get(pindanId);
		if(pinDan == null){
			renderText("找不到这个拼单！");
			return;
		}

		MainService.openAuto(pinDan);
		renderText("切换成功");
	}

	/**
	 * 关闭自动拆单
	 * @throws Exception
	 */
	public void closeAuto() throws Exception {
		String pindanId = getPara("pindanId");
		PinDan pinDan = MainService.getPindanMap().get(pindanId);
		if(pinDan == null){
			renderText("找不到这个拼单！");
			return;
		}

		MainService.closeAuto(pinDan);
		renderText("切换成功");
	}

	/**
	 * 移动订单
	 * @throws Exception
	 */
	public void moveTo() throws Exception {
		String pindanId = getPara("pindanId");
		String orderId = getPara("orderId");
		String fromSbId = getPara("fromSbId");
		String toSbId = getPara("toSbId");

		PinDan pinDan = MainService.getPindanMap().get(pindanId);
		if(pinDan == null){
			renderText("找不到这个拼单！");
			return;
		}

		Order order = MainService.getOrderMap().get(orderId);
		if(order == null){
			renderText("订单不存在");
			return;
		}

		MainService.moveTo(pinDan, fromSbId, toSbId, order);
		renderText("ok");
	}

    /**
     * 修改拼单模式
     * @throws Exception
     */
    public void changePindanMode() throws Exception {
        String pindanId = getPara("pindanId");
        PinDan pinDan = MainService.getPindanMap().get(pindanId);
        if(pinDan == null){
            renderText("找不到这个拼单！");
            return;
        }

        MainService.changePindanMode(pinDan);
        renderText("模式切换成功");
    }

    /**
     * 设置自动结单金额
     * @throws Exception
     */
    public void setAutoStopMoney() throws Exception {
        String pindanId = getPara("pindanId");
        String autoMoney = getPara("autoMoney");
        PinDan pinDan = MainService.getPindanMap().get(pindanId);
        if(pinDan == null){
            renderText("找不到这个拼单！");
            return;
        }

        if(autoMoney == null || !MainService.isNumeric(autoMoney)){
            renderText("请输入正确的整数金额(元)！(输入0代表取消自动结单功能)");
            return;
        }

        MainService.setAutoStopMoney(pinDan, new Integer(autoMoney));
        renderText("设置成功");
    }

    /**
     * 去购物车
     */
    public void toShopCar() throws Exception{
        int opf = getParaToInt(0);
        String key = getPara(1);
        String hallId = getPara(2);
        String pindanId = getPara("pindanId");
        key = URLDecoder.decode(key, "UTF-8");
        if(StringUtil.isBlank(key)){
            renderText("找不到订单");
            return;
        }
        SeparateBill sb = MainService.getSeparateBill(pindanId, key);
        if(sb.getOrderList() == null || sb.getOrderList().isEmpty()){
            renderText("找不到列表");
            return;
        }

        if(OrderPlatform.MEITUAN.equals(OrderPlatform.getOpf(opf))){
            toMeituanShopCart(pindanId, hallId, sb.getOrderList());
        }else{
            toElmShopCart(pindanId, hallId);
        }

        render("/template/relocation.html");
    }

    private void toElmShopCart(String pindanId, String hallId) throws IOException {
        Connection.Response response = Jsoup.connect("http://v5.ele.me/cart/checkout")
                .cookie("cart", "909faf62e6ff11e4b061b82a72dd3244%3Af375ad34cab614dc3c316a368c2a6325")
                .method(Connection.Method.GET).execute();
        response.statusCode();
    }

    private void toMeituanShopCart(String pindanId, String hallId, List<Order> orderList) throws IOException {
        // 生成购物车信息
        JShopCart jShopCart = createShopCart(pindanId, hallId, orderList);
        Connection.Response response = Jsoup.connect("http://waimai.meituan.com/order/shoppingcart")
                .data("shop_cart", JSONObject.toJSONString(jShopCart))
                .ignoreContentType(true)
                .followRedirects(false)
                .method(Connection.Method.POST).execute();
        setAttr("location", response.header("Location"));
    }

    private JShopCart createShopCart(String pindanId, String hallId, List<Order> orderList) {
        JShopCart jShopCart = new JShopCart();
        jShopCart.setPoi(Integer.valueOf(hallId));
        List<JFood> jfoodList = new ArrayList<JFood>();
        for(Order order : orderList){
            JFood jFood = new JFood();
            jFood.setSku(Integer.valueOf(order.getFood().getId()));
            jFood.setNum(1);
            jfoodList.add(jFood);
        }
        jShopCart.setFoods(jfoodList);
        return jShopCart;
    }
}