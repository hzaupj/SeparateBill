'user strict';
var reqorderModule = angular.module('reqorderModule', ['reqorderControllers', 'spbServices', 'ui.bootstrap']);
var reqorderControllers = angular.module('reqorderControllers', ['ngResource']);

//求拼
reqorderControllers.controller('reqorderListCtrl', ['$scope', '$state', '$stateParams', 'Api', '$modal', 'SweetAlert',
    function($scope, $state, $stateParams, Api, $modal, SweetAlert) {
        console.log("注入求拼列表模块");
        $scope.orders = [];
        $scope.curUserInfo = {};
        $scope.hasCurUser = false;
        $scope.hasOwner = false;
        Api.reqorder.list.query({})
        .$promise.then(
            function(data){
                if(data.flag === "SUCCESS"){
                    $scope.orders = data.result.orderList;
                    $scope.shop = data.result.hallMap;
                    $scope.user = data.result.userMap;
                    $scope.myOrders = data.result.myPindanList;
                    //获取当前登录用户
                    var curUser = Api.getAccount();
                    //判断当前用户是否在订单用户中
                    angular.forEach($scope.user, function(item,index){
                        if(item.columns.account == curUser){
                            $scope.hasCurUser = true;
                            $scope.curUserInfo.account = item.columns.account;
                            $scope.curUserInfo.id = item.columns.id;
                        }
                    });
                }else if(data.flag === "UNLOGIN"){
                    $state.go("login");
                }
             },
            function(data) {
                console.log(data.status,"请求出错");
            }
        );

        // 删除求拼单
        $scope.removeReq = function(orderId){
            SweetAlert.swal({   
                title: "删除求拼单",   
                text: "你确定要删除此求拼单吗?",   
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#DD6B55",   
                confirmButtonText: "删除",  
                cancelButtonText: '取消',  
                closeOnConfirm: false   
            }, function(){  
                Api.reqorder.remove.query({
                    orderId: orderId
                })
                .$promise.then(
                    function(data){
                        var flag = data.flag;
                        if(flag === "SUCCESS"){
                            SweetAlert.swal({title:"删除成功!", text:"", type:"success"},function(){
                                $state.go($state.current,{},{reload: true});
                            });
                        }else if(flag === "FAIL"){
                            SweetAlert.swal(data.msg, "", "error");   
                        }
                    },
                    function(data) {
                        SweetAlert.swal("请求出错!", "", "error");   
                    }
                );
            });
        }

        // 将求拼单加入拼单中弹出框
        $scope.animationsEnabled = true;
        $scope.joinReq = function(size,item,hall,list) {
            $scope.item = item;
            $scope.item.hallName = hall;
            // $scope.item.pindan = pindan;
            // $scope.item.pindanHallId = pindanHallId;
            var modalInstance = $modal.open({
                animation: $scope.animationsEnabled,
                templateUrl: 'joinModal.html',
                controller: 'ModalInstanceCtrl',
                size: size,
                resolve: {
                    item: function() {
                        return $scope.item;
                    },
                    list: function() {
                        return $scope.myOrders;
                    }
                }
            });

            modalInstance.result.then(function(item) {
                if(item.food.hallId != item.pindanHallId){
                    swal({
                        title:"加入失败",
                        text:"该求拼的餐厅跟当前拼单的餐厅不对应！",
                        type:"error"
                    });
                    return false;
                }else {
                    Api.reqorder.join.query({
                        orderId: item.id,
                        pindanId: item.pindanId
                    })
                    .$promise.then(
                        function(data) {
                            if(data.flag == "SUCCESS"){
                                swal({
                                    title:"加入成功",
                                    text:"",
                                    type:"success"
                                },function(){
                                    $state.go($state.current,{},{reload: true});
                                });
                            }else{
                                swal({
                                    title:"加入失败",
                                    text: data.msg,
                                    type:"error"
                                });
                            }
                        },
                        function(data) {
                            console.log(data,"请求出错");
                        }
                    );
                }
            }, function() {
                //取消
            });
        };

        

        
    }
]);


reqorderControllers.controller('ModalInstanceCtrl', function ($scope, $modalInstance, item, list) {

    $scope.item = item;
    $scope.list = list;
  // $scope.selected = {
  //   item: $scope.items[0]
  // };
    $scope.submit = function (pindanId, pindanHallId) {
        $scope.item.pindanId = pindanId;
        $scope.item.pindanHallId = pindanHallId;
        $modalInstance.close($scope.item);
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
});

