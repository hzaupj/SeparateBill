/*global ngNice,moment*/
(function () {
    "use strict";
   spbApp.directive('reallyClick', ['$modal',function($modal) {

		var ModalInstanceCtrl = function($scope, $modalInstance) {
			$scope.ok = function() {
				$modalInstance.close();
			};
			$scope.cancel = function() {
			 	$modalInstance.dismiss('cancel');
			};
		};


		return {
			restrict: 'A',
			scope:{
			    reallyClick:"&", //declare a function binding for directive
			    item:"=" //the current item for the directive
			},
			link: function(scope, element, attrs) {
				element.bind('click', function() {
				var message = attrs.reallyMessage || "确定删除吗?";

    			var modalHtml = '<div class="modal-body"><p>' + message + '</p></div>';
				modalHtml += '<div class="modal-footer"><button class="btn btn-danger" ng-click="ok()">确定</button><button class="btn btn" ng-click="cancel()">取消</button></div>';

				var modalInstance = $modal.open({
					template: modalHtml,
					controller: ModalInstanceCtrl,
					size:"sm"
				});

				modalInstance.result.then(function() {
					scope.reallyClick({item:scope.item}); 
					}, function() {
					  //Modal dismissed
				});

			});

		}
      }
    }
  ]);
})();

