angular.callbacks._0({
    "flag": "SUCCESS",
    "result": {
        "orderInfoMap": {
            "492cf2aa-c111-4083-b35d-a5c27ecc1297": [0, 0],
            "b94beb90-c507-48a6-a783-45f5cfbf3ced": [0, 0]
        },
        "pindanMap": {
            "b94beb90-c507-48a6-a783-45f5cfbf3ced": {
                "autoStopMoney": 0,
                "hall": {
                    "bulletinInfo": "1、本店已加盟美团外卖~优先配送美团外卖订单哦~~<br />2、下雨天路滑，送餐如有延迟，请见谅哈！！！<br />3、每份套餐均免费赠送250ml饮料一盒！<br />4、加1元赠送250ml王老吉(不与免费赠饮活动同时进行）！<br />5、加1元赠送300ml可口可乐一瓶(不与免费赠饮活动同时进行）<br />6、加1元赠送300ml雪碧一瓶（不与免费了赠饮活动同时进行！）",
                    "disInfo": "该餐厅支持在线支付(手机客户端专享)新用户首次下单,立减10元\n满30元减8元;满50元减15元(在线支付专享)\n",
                    "disList": [
                        ["30", "8"],
                        ["50", "15"]
                    ],
                    "enable": 1,
                    "id": "152670",
                    "name": "薛大妈",
                    "otherInfo": "起送:￥100 免配送费",
                    "picUrl": "http://p0.meituan.net/208.0/xianfu/66d064fa1f90376f627646e9e5b3ff89335656.jpg",
                    "platform": {
                        "code": 1,
                        "name": "美团外卖",
                        "picUrl": "https://ss0.bdstatic.com/-0U0bnSm1A5BphGlnYG/tam-ogel/bc0d81cae2892fdfe615a8d0642d5e40_121_121.jpg",
                        "url": "http://waimai.meituan.com/home/wssu98hv3m59"
                    },
                    "startPrice": 100
                },
                "hasPublic": true,
                "id": "b94beb90-c507-48a6-a783-45f5cfbf3ced",
                "mode": 1,
                "startDate": 1434100817250,
                "status": 1,
                "userId": 7
            },
            "492cf2aa-c111-4083-b35d-a5c27ecc1297": {
                "autoStopMoney": 0,
                "hall": {
                    "bulletinInfo": "本店已入驻美团外卖，本餐厅活动持续更新尽情期待哦！ 菜品也会不断更新哦！   下雨天路滑，送餐若有延迟，请见谅哈！",
                    "disInfo": "该餐厅支持在线支付(手机客户端专享)新用户首次下单,立减10元\n满30元减10元;满50元减17元(在线支付专享)\n",
                    "disList": [
                        ["30", "10"],
                        ["50", "17"]
                    ],
                    "enable": 1,
                    "id": "207095",
                    "name": "砂锅演义",
                    "otherInfo": "起送:￥30 免配送费",
                    "picUrl": "http://p1.meituan.net/208.0/xianfu/__43635213__7841831.jpg",
                    "platform": {
                        "code": 1,
                        "name": "美团外卖",
                        "picUrl": "https://ss0.bdstatic.com/-0U0bnSm1A5BphGlnYG/tam-ogel/bc0d81cae2892fdfe615a8d0642d5e40_121_121.jpg",
                        "url": "http://waimai.meituan.com/home/wssu98hv3m59"
                    },
                    "startPrice": 30
                },
                "hasPublic": true,
                "id": "492cf2aa-c111-4083-b35d-a5c27ecc1297",
                "mode": 1,
                "startDate": 1434100614044,
                "status": 1,
                "userId": 7
            }
        },
        "userMap": {
            "492cf2aa-c111-4083-b35d-a5c27ecc1297": {
                "columnNames": ["id", "register_time", "floor", "alipay_no", "last_login_time", "name", "account", "weixin_no", "password", "alipay_code_url"],
                "columnValues": [7, 1428539765000, 32, "dsdsa3343", 1428539765000, "123", "123", "321323", "96e79218965eb72c92a549dd5a330112", "3213321312"],
                "columns": {
                    "account": "123",
                    "alipay_code_url": "3213321312",
                    "alipay_no": "dsdsa3343",
                    "floor": 32,
                    "id": 7,
                    "last_login_time": 1428539765000,
                    "name": "123",
                    "password": "96e79218965eb72c92a549dd5a330112",
                    "register_time": 1428539765000,
                    "weixin_no": "321323"
                }
            },
            "b94beb90-c507-48a6-a783-45f5cfbf3ced": {
                "columnNames": ["id", "register_time", "floor", "alipay_no", "last_login_time", "name", "account", "weixin_no", "password", "alipay_code_url"],
                "columnValues": [7, 1428539765000, 32, "dsdsa3343", 1428539765000, "123", "123", "321323", "96e79218965eb72c92a549dd5a330112", "3213321312"],
                "columns": {
                    "account": "123",
                    "alipay_code_url": "3213321312",
                    "alipay_no": "dsdsa3343",
                    "floor": 32,
                    "id": 7,
                    "last_login_time": 1428539765000,
                    "name": "123",
                    "password": "96e79218965eb72c92a549dd5a330112",
                    "register_time": 1428539765000,
                    "weixin_no": "321323"
                }
            }
        }
    }
});
