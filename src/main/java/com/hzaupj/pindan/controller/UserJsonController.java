package com.hzaupj.pindan.controller;

import com.hzaupj.pindan.MainConfig;
import com.hzaupj.pindan.dao.UserDao;
import com.hzaupj.pindan.service.UserService;
import com.jfinal.aop.Before;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.activerecord.tx.Tx;
import jodd.util.StringUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * MainController
 */
public class UserJsonController extends BaseJsonController {


	/**
	 * 登录
	 * @throws Exception
	 */
	@Before(Tx.class)
	public void login() throws Exception {
		String account = getPara("account");
		String password = getPara("password");

		Record user = UserDao.inquireByAccount(account);
		if(user == null) {
            renderJson(ACCOUNT_ERROR, "用户名错误！", null);
            return;
		}

		if(!user.getStr("password").equals(password)){
            renderJson(PASSWORD_ERROR, "密码错误！", null);
			return;
		}

		String sid = UserService.login(user);

		setCookie("sid", sid, MainConfig.MAX_COOKIE_TIME);
		renderOkJson();
	}

	/**
	 * 退出
	 * @throws Exception
	 */
	@Before(Tx.class)
	public void logout() throws Exception {
		removeCookie("sid");
        renderOkJson();
	}


	/**
	 * 注册
	 * @throws Exception
	 */
	@Before(Tx.class)
	public void register() throws Exception {
		String account = getPara("account");
		String name = getPara("name");
		Integer floor = getParaToInt("floor");
		String password = getPara("password");
		String again_password = getPara("again_password");

        if(StringUtil.isEmpty(account)){
            renderErrorJson("工号不能为空！");
            return;
        }
        if(StringUtil.isEmpty(name)){
            renderErrorJson("姓名不能为空！");
            return;
        }

        if(floor == null || floor <=0){
            renderErrorJson("楼层不能为空！");
            return;
        }

        if(StringUtil.isEmpty(password)){
            renderErrorJson("密码不能为空！");
            return;
        }

		if(!password.equals(again_password)){
			renderErrorJson("两次密码输入不一致！");
			return;
		}

		Record user = UserDao.inquireByAccount(account);
		if(user!=null){
            renderErrorJson("该工号已被注册！");
			return;
		}

		String sid = UserService.register(account, name, password, floor);
		setCookie("sid", sid, MainConfig.MAX_COOKIE_TIME);
		renderOkJson();
	}

	/**
	 * 用户信息
	 * @throws Exception
	 */
	public void userInfo() throws Exception {
		Record user = UserService.getUserInfo();
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("account", user.getStr("account"));
        result.put("name", user.getStr("name"));
        result.put("floor", user.getInt("floor"));
        result.put("alipay_no", user.getStr("alipay_no") == null ? "" : user.getStr("alipay_no"));
        result.put("alipay_code_url", user.getStr("alipay_code_url") == null ? "" : user.getStr("alipay_code_url"));
        result.put("weixin_no", user.getStr("weixin_no") == null ? "" : user.getStr("weixin_no"));

		renderOkJson(result);
	}

	/**
	 * 保存用户信息
	 * @throws Exception
	 */
	@Before(Tx.class)
	public void save() throws Exception {
		String account = getPara("account");
		String name = getPara("name");
        Integer floor = getParaToInt("floor");
		String weixinNo = getPara("weixinNo");
		String alipayNo = getPara("alipayNo");
		String alipayCodeUrl = getPara("alipayCodeUrl");

        if(StringUtil.isEmpty(account)){
            renderErrorJson("工号不能为空！");
            return;
        }
        if(StringUtil.isEmpty(name)){
            renderErrorJson("姓名不能为空！");
            return;
        }

        if(floor == null || floor <=0){
            renderErrorJson("楼层不能为空！");
            return;
        }

		Record user = UserDao.inquireByAccount(account);
		if(user == null){
			renderErrorJson("账号不存在！");
			return;
		}

		UserService.save(user,name,alipayNo,weixinNo,alipayCodeUrl,floor);

		renderOkJson("修改完成！");
	}

	/**
	 * 修改密码
	 * @throws Exception
	 */
	@Before(Tx.class)
	public void updatePassword() throws Exception {
		String account = getPara("account");
		String old_password = getPara("old_password");
		String new_password = getPara("new_password");
		String new2_password = getPara("new2_password");

		if(!new_password.equals(new2_password)){
            renderErrorJson("两次新密码输入不一致！");
			return;
		}

		Record user = UserDao.inquireByAccount(account);
		if(user == null){
            renderErrorJson("账号不存在！");
			return;
		}

		if(!old_password.equals(user.getStr("password"))){
			renderErrorJson("旧密码输入错误！");
			return;
		}

		UserService.updatePassword(user, new_password);

		renderOkJson("密码修改成功！");
	}

}





