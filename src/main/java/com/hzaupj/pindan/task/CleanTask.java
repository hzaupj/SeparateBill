package com.hzaupj.pindan.task;

import com.hzaupj.pindan.service.MainService;
import com.hzaupj.pindan.vo.PinDan;
import org.joda.time.DateTime;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.Map;

/**
 * @Description: MX_N
 * @author: pengjian_91
 * @date: 2015/4/27
 * @version: V1.0
 */
public class CleanTask implements Job {

	private static Logger logger = LoggerFactory.getLogger(CleanTask.class);

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        Map<String, PinDan> pinDanMap = MainService.getPindanMap();
        Collection<PinDan> values = pinDanMap.values();
		logger.info("清理无效的拼单 start " + DateTime.now().toString("yyyy-MM-dd HH:mm:ss"));
        for (PinDan pinDan : values){
            DateTime startDate = new DateTime(pinDan.getStartDate());
            DateTime nowDate = DateTime.now();
            // 停止点餐并且拼单时间超过1天的拼单要删除
            if(pinDan.getStatus() == 0 && startDate.plusDays(1).compareTo(nowDate)<0){
                System.out.println("无效的拼单id = " + pinDan.getId());
                MainService.deletePindan(pinDan.getId());
            }
        }
		logger.info("清理无效的拼单 end " + DateTime.now().toString("yyyy-MM-dd HH:mm:ss"));
    }
}
