'use strict';

/* Services */

var spbServices = angular.module('spbServices', ['ngResource']);
spbServices.factory('Api', ['$resource', '$rootScope', '$localStorage', function($resource, $rootScope, $localStorage) {
	var baseUrl = '',
         cb = '?&callback=JSON_CALLBACK';
    var factory = {
        platform : {
            list: $resource('data/platform.json', {} , {})
        },
        shop : {
            list: $resource('', {} , {
                query: {
                    // url: "data/hallList.json",
                    url: baseUrl+'/main/hallList/:platformId'+cb,
                    method: 'JSONP',
                    params: {
                        platformId: $localStorage.platformId
                    },
                    isArray: false
                }
            }),
            detail: $resource('', {} , {
                query: {
                    url: baseUrl+'/main/foodList/:platformId?hallId'+cb,
                    method: 'JSONP',
                    isArray: false
                }
            })
        },
        user: {
            login: $resource('', {}, {
                query: {
                    url: baseUrl+'/user/login'+cb,
                    method: 'JSONP',
                    isArray: false
                }
            }),
            logout: $resource('', {}, {
                query: {
                    url: baseUrl+'/user/logout'+cb,
                    method: 'JSONP',
                    isArray: false
                }
            }),
            register: $resource('', {}, {
                query: {
                    url: baseUrl+'/user/register'+cb,
                    method: 'JSONP',
                    isArray: false
                }
            }),
            userbasic: $resource('', {}, {
                query: {
                    url: baseUrl+'/user/userInfo'+cb,
                    method: 'JSONP',
                    isArray: false
                }
            }),
            update: $resource('', {}, {
                query: {
                    url: baseUrl+'/user/save'+cb,
                    method: 'JSONP',
                    isArray: false
                }
            }),
            updatePwd: $resource('', {}, {
                query: {
                    url: baseUrl+'/user/updatePassword'+cb,
                    method: 'JSONP',
                    isArray: false
                }
            })
        },
        reqorder : {
            list: $resource('', {} , {
                query: {
                    url: baseUrl+'/main/requestShareList'+cb,
                    method: 'JSONP',
                    isArray: false
                }
            }),
            add: $resource('', {} , {
                query: {
                    url: baseUrl+'/main/addRequestShareOrder'+cb,
                    method: 'JSONP',
                    isArray: false
                }
            }),
            remove: $resource('', {} , {
                query: {
                    url: baseUrl+'/main/deleteReq'+cb,
                    method: 'JSONP',
                    isArray: false
                }
            }),
            join: $resource('', {} , {
                query: {
                    url: baseUrl+'/main/joinPindan'+cb,
                    method: 'JSONP',
                    isArray: false
                }
            })
        },
        order: {
            add: $resource('', {}, {
                query: {
                    url: baseUrl+'/main/addPindan/:platformId'+cb,
                    method: 'JSONP',
                    params: {
                        platformId: '@platformId'
                    },
                    isArray: false
                }
            }),
            remove: $resource('', {}, {
                query: {
                    url: baseUrl+'/main/deletePindan?pindanId'+cb,
                    method: 'JSONP',
                    params: {
                        pindanId: '@pindanId'
                    },
                    isArray: false
                }
            }),
            book: $resource('', {}, {
                query: {
                    url: baseUrl+'/main/foodList/:platformId?hallId?pindanId?hasPublic'+cb,
                    method: 'JSONP',
                    params: {
                        platformId: '@platformId'
                    },
                    isArray: false
                }
            }),
            quota: $resource('', {}, {
                query: {
                    url: baseUrl+'/main/setAutoStopMoney?pindanId?autoMoney'+cb,
                    method: 'JSONP',
                    isArray: false
                }
            }),
            mode: $resource('', {}, {
                query: {
                    url: baseUrl+'/main/changePindanMode'+cb,
                    method: 'JSONP',
                    params: {
                        pindanId: '@pindanId'
                    },
                    isArray: false
                }
            }),
            start: $resource('', {}, {
                query: {
                    url: baseUrl+'/main/start?pindanId'+cb,
                    method: 'JSONP',
                    // params: {
                    //     pindanId: '@pindanId'
                    // },
                    isArray: false
                }
            }),
            stop: $resource('', {}, {
                query: {
                    url: baseUrl+'/main/stop?pindanId'+cb,
                    method: 'JSONP',
                    params: {
                        pindanId: '@pindanId'
                    },
                    isArray: false
                }
            }),
            list: $resource('', {}, {
                query: {
                    // url: 'data/pindanList.json',
                    url: baseUrl+'/main/pindanList/:stateId'+cb,
                    method: 'JSONP',
                    params: {
                        stateId: '@stateId'
                    },
                    isArray: false
                }
            }),
            detail: $resource('', {}, {
                query: {
                    // url: "data/orderDetail.json",
                    url: baseUrl+'/main/report/:platformId?hallId?pindanId'+cb,
                    method: 'JSONP',
                    params: {
                        platformId: '@platformId'
                    },
                    isArray: false
                }
            }),
            addFood: $resource('', {}, {
                query: {
                    url: baseUrl+'/main/order/:platformId?hallId?pindanId?foodId'+cb,
                    method: 'JSONP',
                    params: {
                        platformId: '@platformId'
                    },
                    isArray: false
                }
            }),
            removeFood: $resource('', {}, {
                query: {
                    url: baseUrl+'/main/delete?orderId'+cb,
                    method: 'JSONP',
                    params: {
                        orderId: '@orderId'
                    },
                    isArray: false
                }
            }),
            updateRemark: $resource('', {}, {
                query: {
                    url: baseUrl+'/main/updateRemark'+cb,
                    method: 'JSONP',
                    isArray: false
                }
            }),
            addSB: $resource('', {}, {
                query: {
                    url: baseUrl+'/main/addSB'+cb,
                    method: 'JSONP',
                    isArray: false
                }
            }),
            deleteSB: $resource('', {}, {
                query: {
                    url: baseUrl+'/main/deleteSB'+cb,
                    method: 'JSONP',
                    isArray: false
                }
            }),
            moveSB: $resource('', {}, {
                query: {
                    url: baseUrl+'/main/moveTo'+cb,
                    method: 'JSONP',
                    isArray: false
                }
            }),
            closeAuto: $resource('', {}, {
                query: {
                    url: baseUrl+'/main/closeAuto'+cb,
                    method: 'JSONP',
                    isArray: false
                }
            }),
            openAuto: $resource('', {}, {
                query: {
                    url: baseUrl+'/main/openAuto'+cb,
                    method: 'JSONP',
                    isArray: false
                }
            })
        },
        isLoggedIn: function () {
          return $localStorage.loggedIn;
        },
        getAccount: function() {
          return $localStorage.account;
        },
        getPlatformId: function() {
          return $localStorage.platformId;
        },
        clear: function () {
          $localStorage.account = null;
          $localStorage.loggedIn = false;
          $localStorage.platformId = null;
        }
        
    };
    $rootScope.gobal.getAccount = factory.getAccount;
    $rootScope.gobal.clear = factory.clear;
    $rootScope.gobal.isLoggedIn = factory.isLoggedIn;

    // $rootScope.gobal.getAccount = factory.user.getAccount;
    return factory;

}]);

