package com.hzaupj.test;

import com.hzaupj.pindan.service.MainService;
import com.hzaupj.pindan.task.CleanReqShareList;
import com.hzaupj.pindan.vo.Order;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.Test;

/**
 * @Description: MX_N
 * @author: pengjian_91
 * @date: 2015/4/3
 * @version: V1.0
 */
public class TestShop {
    @Test
    public void testShoppingcart() throws Exception {
        Document response = Jsoup.connect("http://v5.ele.me/cart/checkout")
                //                .userAgent("User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML,
                // like Gecko) Chrome/41.0.2272.118 Safari/537.36")
                //                        .followRedirects(false)
                //                .header("Content-Type","text/plain;charset=UTF-8")
                //                .data("track_id",
                //                        "1431413689%7C93cca13ced9d3ef75c98a096cc33d264019de866d5fa6f2933
                // %7C572fcd4d59d53806a9431bc1944b9ada")
                //        Connection.Response doc = Jsoup.connect("http://r.ele.me/fz-rjyyf")
                .ignoreContentType(true)
//                .followRedirects(false)
                        //                .cookie("cart",
                        // "3b49550af86f11e4b44744a842037e79%3Aaf404fd673deafc1c33cc05e61c5569c")
                        //                .cookie("eleme__ele_me",
                        // "b4bceb9c124dd0dfd90c6c07a41bb3ac%3A463869bc6446bbf5358f0c849bb9653e360ca1f7")
                        //                .cookie("_ga","GA1.2.464202098.1431413691")
                .cookie("cart", "c9984c2cf87311e486c4b083fed81775%3A4421292e6683837b3aafd43e17de1e6f")
                .cookie("eleme__ele_me", "3c21d32770b2b5e271c17c7377834130%3Af26cc75f7af7735e86891ccf6e3bb86d71127187")
                        //                .cookie("track_id",
                        // "1431413689%7C93cca13ced9d3ef75c98a096cc33d264019de866d5fa6f2933
                        // %7C572fcd4d59d53806a9431bc1944b9ada")
                        //                .cookie("track_id",
                        // "1431413689%7C93cca13ced9d3ef75c98a096cc33d264019de866d5fa6f2933
                        // %7C572fcd4d59d53806a9431bc1944b9ada")
                        //                .cookie("ubt_ssid","n6xiwo7in759kn60ll4ehakneqmgde0y_2015-05-12")

                .get();
        System.out.println(response);

    }

    @Test
    public void testFoodList() throws Exception {
        MainService.getReqShareOrderMap().put("1", new Order());
        MainService.getReqShareOrderMap().put("2", new Order());
        CleanReqShareList cleanReqShareList = new CleanReqShareList();
        cleanReqShareList.execute(null);
        System.out.println(MainService.getReqShareOrderMap().size());
    }
}
